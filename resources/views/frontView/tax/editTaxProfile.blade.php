@extends('frontView.masterView')

@section('main_body')

<style>
  .avatar {
/*     display: block; */
    border-radius: 200px;
    box-sizing: border-box;
    background-color: #DDD;
    margin:auto;

}
  
    .checkbox-list .checkbox {
    margin-bottom: 0rem;
}
  .box-shadow {
/*     box-shadow: 0 5px 15px 2px rgba(0, 0, 0, 0.1); */
/*     height: 300px; */
}
  .card.card-custom:hover {
    box-shadow: 0 5px 25px 2px rgba(0, 0, 0, 0.1);
}
.edit-saveChanges{
    position: absolute;
top: 69px;
height:50px;
}
.edit-saveChanges2{
   margin-top: 45px !important;
    height: 47px;
}
  .ki-check:before{
      display:none;
  }
  .wizard.wizard-6 .wizard-content .wizard-nav .wizard-steps .wizard-step[data-wizard-state="done"] .wizard-icon .wizard-number{
      color: #1BC5BD !important;
      display:block !important;
  }
  .wizard.wizard-6 .wizard-content .wizard-nav .wizard-steps .wizard-step[data-wizard-state="current"]:last-child .wizard-icon .wizard-number{
      color: #1BC5BD !important;
      display:block !important;
  }
  @media screen and (max-width:767px){
    
    .edit-saveChanges {
    position: absolute;
    top: 63px;
    height: 48px;
    left:0px;
}
.ml-10{
    margin-left:0px !important;
}
}
</style>

<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="mt-10 mb-10 container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard 6-->
                <div class="wizard wizard-6 d-flex flex-column flex-column-fluid" id="kt_wizard">
                    <!--begin::Container-->
                    <div class="wizard-content d-flex flex-column ml-10 mr-10">
                        <!--begin::Nav-->
                        <div class="d-flex flex-column-auto flex-column px-md-0 px-0">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav pb-lg-10 pb-3 d-flex flex-column align-items-center align-items-md-start">
                                <!--begin::Wizard Steps-->
                                <div class="wizard-steps d-flex flex-column flex-md-row">


                                    
                                    <!--begin::Wizard Step 1 Nav-->
                                    <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step" data-wizard-state="current">
                                        <div class="wizard-wrapper pr-lg-7 pr-5">
                                            <div class="wizard-icon">
                                                <i class="wizard-check ki ki-check"></i>
                                                <span class="wizard-number">1</span>
                                            </div>
                                            <div class="wizard-label mr-3">
                                                <div class="wizard-desc">Personal details</div>
                                            </div>
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
                                                        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 1 Nav-->



                                    <!--begin::Wizard Step 2 Nav-->
                                    <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                                        <div class="wizard-wrapper pr-lg-7 pr-5">
                                            <div class="wizard-icon">
                                                <i class="wizard-check ki ki-check"></i>
                                                <span class="wizard-number">2</span>
                                            </div>
                                            <div class="wizard-label mr-3">
                                                <div class="wizard-desc">Income Type</div>
                                            </div>
                                            <span class="svg-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
                                                        <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 2 Nav-->


                                    <!--begin::Wizard Step 3 Nav-->
                                    <div class="wizard-step flex-grow-1 flex-basis-0" data-wizard-type="step">
                                        <div class="wizard-wrapper">
                                            <div class="wizard-icon">
                                                <i class="wizard-check ki ki-check"></i>
                                                <span class="wizard-number">3</span>
                                            </div>
                                            <div class="wizard-label">
                                                <div class="wizard-desc">Profile Completed</div>
                                                <div class="wizard-desc">Submit form</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 3 Nav-->
                                </div>
                                <!--end::Wizard Steps-->
                            </div>
                            <!--end: Wizard Nav-->
                        </div>
                        <!--end::Nav-->

                        <!--begin::Form-->
                        <form class=".sasaas" method="post" action="/updateTaxProfile" novalidate="novalidate" id="kt_wizard_form">
                            {{ csrf_field() }}
                            <!--begin: Wizard Step 1-->
                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                <!--begin::Title-->
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <h3 class="mb-10 font-weight-bold text-dark"><u>You are editing, <?= $client->fname.' '.$client->lname ?> details...</u></h3>
                                    </div>
                                  <input type="hidden" name="updateUserId" value="{{$id}}">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="form-group col-md-6 col-sm-12">
                                                <label>First Name</label>
                                                <input value="<?= $client->fname ?>" type="text" id="fname" name="fname" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >
                                            </div>
                                            <div class="form-group col-md-6 col-sm-12">
                                                <label>Last Name</label>
                                                <input value="<?= $client->lname ?>" type="text" id="lname" name="lname" placeholder="Last Name" required class="form-control form-control-solid form-control-lg" >
                                                <span class="form-text text-muted">Please enter the lase name from social security card.</span>
                                            </div>
                                        </div>
                                      <div class="row">
                                        <div class="form-group col-md-6 col-sm-12">
                                            <label>Date OF Birth</label>
                                            <input value="<?= $client->dob ?>" type="date" id="dob" name="dob" placeholder="Date of birth" required class="form-control form-control-solid form-control-lg" >
                                            <span class="form-text text-muted">mm/dd/yyyy</span>
                                        </div>
                                      </div>
                                    </div>
                                    
                                </div>


                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <h3 class="mb-10 font-weight-bold text-dark"></h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group fv-plugins-icon-container">
                                            <label>Martial Status</label>
                                            <div class="row">
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input @if($client->msSingle == 1) checked @endif  onchange="changeChk(this)" type="checkbox" id="chkSingle" name="chkSingle" >
                                                    <span></span>&nbsp;&nbsp; Single
                                                </label>
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input @if($client->msMfj == 1) checked @endif onchange="changeChk(this)" type="checkbox" id="chkMfj" name="chkMfj" >
                                                    <span></span>&nbsp;&nbsp; Married filling jointly
                                                </label>
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input @if($client->msMfs == 1) checked @endif onchange="changeChk(this)" type="checkbox" id="chkMfs" name="chkMfs" >
                                                    <span></span>&nbsp;&nbsp; Married filling Separately(MFS)
                                                </label>
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input @if($client->msHoh == 1) checked @endif onchange="changeChk(this)" type="checkbox" id="chkHoh" name="chkHoh" >
                                                    <span></span>&nbsp;&nbsp; Head of household(HOH)
                                                </label>
                                                <label class="checkbox ml-5 mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                    <input @if($client->msQw == 1) checked @endif onchange="changeChk(this)" type="checkbox" id="chkQw" name="chkQw" >
                                                    <span></span>&nbsp;&nbsp; Qualifying widow (QW)
                                                </label>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                                <script>
                                    function changeChk(obj){
                                        $('#chkQw').not(obj).prop('checked', false);
                                        $('#chkHoh').not(obj).prop('checked', false);
                                        $('#chkMfs').not(obj).prop('checked', false);
                                        $('#chkMfj').not(obj).prop('checked', false);
                                        $('#chkSingle').not(obj).prop('checked', false);
                                        if(obj.id == 'chkSingle'){
                                            $('#spouse_div').hide();
                                            $('#family_div').hide();
                                        }else{
                                            $('#spouse_div').show();
                                            $('#family_div').show();
                                        }
                                    }

                                </script>
                                <div id="spouse_div" class="row mt-10">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container">
                                                <label>Spouse First Name</label>
                                                <input value="{{$client->sfname}}" type="text" name="sfname" id="sfname" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >
                                            </div>
                                            <div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container">
                                                <label>Spouse Last Name</label>
                                                <input value="{{$client->slname}}" type="text" name="slname" id="slname" placeholder="Last Name" required class="form-control form-control-solid form-control-lg" >
                                                <span class="form-text text-muted">Please enter the lase name from social security card.</span>
                                            </div>
                                        </div>
                                      <div class="row">
                                        <div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container">
                                            <label>Date OF Birth</label>
                                            <input value="{{$client->sdob}}" type="date" name="sdob" id="sdob" placeholder="Date of birth" required class="form-control form-control-solid form-control-lg" >
                                            <span class="form-text text-muted">mm/dd/yyyy</span>
                                        </div>
                                      </div>
                                    </div>
                                    
                                </div>

                                <div class="row mt-10">
                                    <div class="col-md-12 col-sm-12">
                                        <h2 class="mb-10 font-weight-bold text-dark">Address</h2>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                        <div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container">
                                            <label>Country</label>
                                            <select  name="country" id="country" class="form-control form-control-solid form-control-lg">
                                                @foreach($allCountry as $con)
                                                    <option @if($con == $client->country) selected @endif value="{{$con}}">{{$con}}</option>
                                                  @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-sm-12 form-group fv-plugins-icon-container">
                                            <label>State</label>
                                            <input value="{{ $client->state }}" type="text" name="state" id="state" placeholder="State" required class="form-control form-control-solid form-control-lg" >
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div id="doc_div">
                                            <div class="row">
                                                <div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container">
                                                    <label>City</label>
                                                    <input value="{{ $client->city }}" type="text" name="city" id="city" placeholder="City" required class="form-control form-control-solid form-control-lg" >
                                                </div>
                                                <div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container">
                                                    <label>Address</label>
                                                    <input value="{{ $client->address }}" type="text" name="address" id="address" placeholder="Address" required class="form-control form-control-solid form-control-lg" >
                                                </div>
                                        </div>                                            
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="row mt-10">
                                    <div class="col-md-12 col-sm-12">
                                        <h2 class="mb-10 font-weight-bold text-dark ">Citizenship...</h2>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                      <?php $client->citizenship = json_decode($client->citizenship); ?>
                                        <div id="citizen_div">
                                            <div class="form-group fv-plugins-icon-container mb-0">
                                                <label>Citizenship</label>
                                              <?php $cityIndex = 0 ?>
                                                @foreach ($client->citizenship as $city)
                                                  <div id="cit_new_div{{$city}}">
                                                    @if($cityIndex != 0)
                                                    <div style="padding:0!important; margin:0; text-align:right" class="col-md-12 mt-5" >
                                                      <a class="delete-repeater my-3" onclick="removeCitizen('{{$city}}')" style="cursor:pointer"><b>X</b></a>
                                                      <select id="citizenship" name="citizenship[]" class="form-control form-control-solid form-control-lg mb-0">
                                                          <option value="">Select Country</option>
                                                          @foreach($allCountry as $con)
                                                            <option @if($con == $city) selected @endif value="{{$con}}">{{$con}}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                    @else
                                                      <select id="citizenship" name="citizenship[]" class="form-control form-control-solid form-control-lg mb-0">
                                                          <option value="">Select Country</option>
                                                          @foreach($allCountry as $con)
                                                            <option @if($con == $city) selected @endif value="{{$con}}">{{$con}}</option>
                                                          @endforeach
                                                      </select>
                                                    @endif
                                                    <?php $cityIndex++ ?>
                                                  </div>
                                                 @endforeach
                                            </div>
                                        </div>
                                        <a style="cursor:pointer" onclick="addMoreCitizenship()" class="float-right mt-3" ><u>Add More</u></a>
                                    </div>
                                    <script>
                                        var citizen = 0;
                                        function removeCitizen(id){
                                          console.log(id);
                                            $('#cit_new_div'+id).remove();
                                        }
                                        function addMoreCitizenship(){
                                            citizen++;
                                            $('#citizen_div').append('<div id="cit_new_div'+citizen+'" style="padding:0!important; margin:0" class="row col-md-12 col-sm-12">'+
                                              '<div style="padding:0!important; margin:0" class="row col-md-12 col-sm-12">'+
                                            '<div style="padding:0!important; margin:0; text-align:right" class="col-md-12 my-3" ><a class="delete-repeater my-3" onclick="removeCitizen('+citizen+')" style="cursor:pointer"><b>X</b></a></div>'+
                                                    '</div>'+
                                            '<div style="padding:0!important" class="form-group col-md-12 fv-plugins-icon-container">'+
                                            '<select name="citizenship[]" class="form-control form-control-solid form-control-lg">'+
                                            '<option>Select Country</option>'+
                                                    @foreach($allCountry as $con)
                                                      '<option value="{{$con}}">{{$con}}</option>'+
                                                    @endforeach
                                            '</select></div></div>');
                                        }
                                    </script>

                                </div>

                                <div style="padding:0!important; margin:0" class="row mt-10">
                                    <div style="padding:0!important; margin:0" class="col-md-12 col-sm-12">
                                        <h2 class="mb-10 font-weight-bold text-dark">Dependents</h2>
                                    </div>
                                        <div style="padding:0!important; margin:0" class="row col-md-12 col-sm-12">
                                            <div style="padding:0!important; margin:0" id="dependent_div" class="row col-md-12 col-sm-12">
                                              <?php $depIndex = 0 ?>
                                              @foreach ($txtProfileDepList as $dep)
                                              <div class="row col-md-12 pr-sm-3 pr-0" id="dep_new_div{{$dep->id}}">
                                                @if($depIndex != 0)
                                                <div style="padding:0!important; margin:0; text-align:right" class="col-md-12 mt-5 mb-3" >
                                                      <a class="delete-repeater my-3" onclick="removeDiv('{{$dep->id}}')" style="cursor:pointer"><b>X</b></a>
                                                </div>
                                                @endif
                                                
                                                <div style="padding:0!important; margin:0" class="col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container pr-sm-3 pr-0">
                                                            <label>First Name</label>
                                                            <input value="{{$dep->fname}}" type="text" name="dfname[]" id="dfname" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >
                                                        </div>
                                                        <div class="col-md-6 col-sm-12 pr-sm-3 pr-0">
                                                            <div id="doc_div">
                                                                    <div class="form-group fv-plugins-icon-container">
                                                                        <label>Last Name</label>
                                                                        <input value="{{$dep->lname}}" type="text" name="dlname[]" id="dlname" placeholder="Last Name" required class="form-control form-control-solid form-control-lg" >
                                                                    </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                  <div class="row">
                                                    <div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container pr-sm-3 pr-0">
                                                        <label>Relationship to you</label>
                                                        <input value="{{$dep->relation}}" type="text" name="dRelationship[]" id="dRelationship" placeholder="Relationship to you" required class="form-control form-control-solid form-control-lg" >
                                                    </div>
                                                  </div>
                                                  <?php
                                                            $chkCrd = '';
                                                            $chkOthrt = '';
                                                            if($dep->child_tax_credit == 1){
                                                              $chkCrd = 'checked';
                                                            }
                                                            if($dep->child_for_other == 1){
                                                              $chkOthrt = 'checked';
                                                            }
                                                         ?>
                                                    <div class="form-group fv-plugins-icon-container pr-sm-3 pr-0">
                                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                            <input <?= $chkCrd ?> type="checkbox" name="chkChildTaxCredit<?= $depIndex ?>" id="chkChildTaxCredit" >
                                                            <span></span>&nbsp;&nbsp; Child tax credit
                                                        </label>
                                                    </div>
                                                    <div class="form-group fv-plugins-icon-container pr-sm-3 pr-0">
                                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                                            <input <?= $chkOthrt ?> type="checkbox" name="chkChildForOthrt<?= $depIndex ?>" id="chkChildForOthrt" >
                                                            <span></span>&nbsp;&nbsp; Child for other dependents
                                                        </label>
                                                    </div>
                                                  </div>
                                                </div>
                                              <?php $depIndex++; ?>
                                              <input type="hidden" name="dependentCount" id ="dependentCount" value="<?= $depIndex ?>" >
                                              @endforeach
                                          </div>
                                          <div class="col-md-6" >
                                            <button type="submit" class="btn btn-success edit-saveChanges">
                                              Save Changes
                                            </button>
                                          </div>
                                            <div class="col-md-6" >
                                                <a style="cursor:pointer" onclick="addDependent()" class="float-right" ><u>Add More</u></a>
                                            </div>
                                        </div>
                                    <script>
                                        var depTab = 0;
                                        function removeDiv(id){
                                             $('#dep_new_div'+id).remove();
                                            depTab = $('#dependentCount').val();
                                            depTab = parseInt(depTab)-1;
                                            $('#dependentCount').val(depTab);
//                                              depTab = depTab -1;
                                            //  for(var i = id; i<= depTab; i++){
                                            //     $('#dep_div_lbl'+i).html(i);
                                            //  }
                                        }
                                        function addDependent(){
                                            depTab = $('#dependentCount').val();
                                            depTab = parseInt(depTab)+1;
                                            $('#dependentCount').val(depTab);
                                            $('#dependent_div').append('<div style="padding:0!important; margin:0" id="dep_new_div'+depTab+'" class="row col-md-12 col-sm-12 p-0"> <div class="row col-md-12 col-sm-12 p-0">'+
                                            '<div style="text-align:right; padding:0!important; margin:0" class="col-md-12" ><a class="delete-repeater my-3" onclick="removeDiv('+depTab+')" style="cursor:pointer"><b>X</b></a></div>'+
                                                    '</div>'+
                                            '<div style="padding:0!important" class="pt-5 col-md-12 col-sm-12">'+
                                                    '<div class="row">'+
                                                        '<div class="form-group col-md-6 col-sm-12 fv-plugins-icon-container">'+
                                                            '<label>First Name</label>'+
                                                            '<input type="text" name="dfname[]" id="dfname" placeholder="First Name" required class="form-control form-control-solid form-control-lg" >'+
                                                        '</div>'+
                                                        '<div class="col-md-6 col-sm-12">'+
                                                            '<div id="doc_div">'+
                                                                    '<div class="form-group fv-plugins-icon-container">'+
                                                                        '<label>Last Name</label>'+
                                                                        '<input type="text" name="dlname[]" id="dlname" placeholder="Last Name" required class="form-control form-control-solid form-control-lg" >'+
                                                                    '</div>'+
                                                            '</div>'+
                                                        '</div> '+
                                                    '</div>'+
                                                    '<div style="padding:0!important" class="form-group col-md-6 col-sm-12 fv-plugins-icon-container">'+
                                                        '<label>Relationship to you</label>'+
                                                        '<input type="text" name="dRelationship[]" id="dRelationship" placeholder="Relationship to you" required class="form-control form-control-solid form-control-lg" >'+
                                                    '</div>'+
                                                    '<div class="form-group fv-plugins-icon-container">'+
                                                        '<label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">'+
                                                            '<input type="checkbox" name="chkChildTaxCredit'+depTab+'" id="chkChildTaxCredit" >'+
                                                            '<span></span>&nbsp;&nbsp; Child tax credit'+
                                                        '</label>'+
                                                    '</div>'+
                                                    '<div class="form-group fv-plugins-icon-container">'+
                                                        '<label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">'+
                                                            '<input type="checkbox" name="chkChildForOthrt'+depTab+'" id="chkChildForOthrt" >'+
                                                            '<span></span>&nbsp;&nbsp; Child for other dependents'+
                                                        '</label>'+
                                                    '</div>'+
                                                '</div></div>');
                                        }
                                    </script>
                                </div>


                            </div>
                            <!--end: Wizard Step 1-->


                            <!--begin: Wizard Step 2-->
                            <div class="pb-5" data-wizard-type="step-content">
                                <div class="ml-10 row">
                                    <div class="col-md-12 col-sm-12">
                                        <h2 class="mb-10 font-weight-bold text-dark">Enter employment details, select the income type applied to the client...</h2>
                                    </div>
                                    <div class="row col-md-12 pr-md-3 pr-0">
                                        <input type="hidden" name="allCountryIds" value='<?= json_encode($countryListIds) ?>'>
                                    <?php $counter = 0; foreach ($incomeList as $income){ ?>
                                      <div class="box-shadow mt-5 col-xl-4">
                                          <!--begin::Stats Widget 1-->
                                          <div class="card shadow-md card-custom bgi-no-repeat gutter-b">
                                            <!--begin::Body-->
                                            <div style="padding:0px;" class="card-body row">
                                              <div style="text-align: center" class="row mt-2 col-md-6">
                                                <div style="text-align: center" class="col-md-12 ">
                                                </div>
                                                <img class="avatar" width="100px" height="100px" src="{{ url('storage/app/'.$income->img_url)}}">
                                                  <label style="text-align: center;" class="col-md-12 income-name mt-2"><b>{{ $income->name}}</b></label>
                                                </div>
                                                <div class="checkbox-list ml-4 col-md-6 mt-5">
                                                 <?php $index = 0; ?>
                                                    @foreach ($income->country as $country)
                                                       <label class="checkbox mt-2 checkbox-outline checkbox-outline-2x checkbox-primary" style="font-size:10px;">
                                                            <input type="hidden" id="incid<?= $income->id.'-'.$country->country_id ?>" name="incid<?= $income->id.'-'.$country->country_id ?>" value="<?= $income->id.'-'.$country->country_id ?>">
                                                            <input type="hidden" id="incName<?= $income->id.'-'.$country->country_id ?>" name="incName<?= $income->id.'-'.$country->country_id ?>" value="<?= $income->name ?>">
                                                            <input type="hidden" id="incDocx<?= $income->id.'-'.$country->country_id ?>" name="incDocx<?= $income->id.'-'.$country->country_id ?>" value='<?=  $income->country_docx[$index] ?>'>
                                                            <input type="hidden" id="countryName<?= $income->id.'-'.$country->country_id ?>" name="countryName<?= $income->id.'-'.$country->country_id ?>" value="<?= $country->country_name ?>">
                                                         <?php
                                                          $chk = '';
                                                          foreach($txtProfileDocList as $tpd){
                                                            if($tpd->income_id == $income->id && $tpd->country_id == $country->country_id){
                                                              $chk = 'checked';
                                                            }
                                                          }
                                                         ?>
                                                            <input <?= $chk ?> onchange="handleChange(event)" type="checkbox" id="<?= $income->id.'-'.$country->country_id ?>" name="inc<?= $income->id.'-'.$country->country_id ?>" >
                                                            <span></span><?= $country->country_name ?>
                                                         
                                                        </label>
                                                        <?php $index++; ?>
                                                    @endforeach
                                                </div>
                                                <div class="col-md-12 mt-3">
                                                     
                                                </div>
                                            </div>
                                            <!--end::Body-->
                                          </div>
                                          <!--end::Stats Widget 1-->
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                              <div class="col-md-12" >
                                            
                                          </div>
                            </div>
                            <script>
                                function handleChange(e){
                                    if(e.target.checked) {
                                        var id = e.target.id;
                                        var incomeName = $('#incName'+id).val();
                                        var countryName = $('#countryName'+id).val();
                                        var docx = $('#incDocx'+id).val();
                                        var obj = JSON.parse(docx);
                                        var docList = '<ul>';
                                        var ind = 0;
                                        for (val of obj) {
                                          docList += '<li id="inpDox'+ind+id+'"><div class="mb-1 input-group"><input class="form-control" name="'+id+'input[]" type="text" value="'+val["docs"]+'">'+
                                                '<div class="input-group-append">'+
                                                '<a class="delete-repeater my-2" onclick="removeField(\''+ind+id+'\')" class="btn btn-outline-primary" type="button">'+
                                                 '<b>X</b></a>'+
                                              '</div><div></li>';
                                          ind++;
                                        }
                                      docList +='</ul>';
                                        $('#tblIncome').append('<tr id="tblRow'+id+'"><td>'+incomeName+'</td><td>'+countryName+'</td><td>'+docList+'</td></tr>');
                                        console.log(incomeName);
                                    } else {
                                        var id = e.target.id;
                                        $('#tblRow'+id).remove();
                                    }
                                }
                            </script>
                            <!--end: Wizard Step 2-->

                            <!--begin: Wizard Step 3-->
                            <div class="pb-5" data-wizard-type="step-content">
                                <div style="text-align: center;" class="row mx-0">
                                    <div style="height: 220px" style="text-align: center; border-color: #000;" class="col-md-5 mt-10 col-sm-12 card second-card">
                                            <h3 class="mt-md-5 mt-2">Client Personal details</h3>
                                            {{-- <span class="mb-10 form-text text-muted">Want to edit personal Detail <a data-wizard-type="step" data-wizard-state="current" href="#"><u>Click here</u></a>.</span> --}}
                                            <div class="row mt-md-5 mt-2">
                                                <div class="col-md-4">
                                                    <img height="100px" src="{{asset('img/person.PNG')}}">
                                                </div>
                                                <div class="col-md-8">
                                                    <div style="text-align: left" class="mt-4 ml-2">
                                                    <ul>
                                                        <li><label id="m_name"><?= $client->fname.' '.$client->lname ?></label></li>
                                                        <li><label id="m_dob"></label><?= $client->dob ?></li>
                                                        <li><label id="m_relation"></label><?= $client->city ?></li>
                                                    </ul>
                                                </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div id="family_div" style="height: 220px" style="text-align: center; border-color: #000;" class="offset-md-2 col-md-5 mt-10 col-sm-12 card second-card">
                                        <div>
                                        <h3 class="mt-md-5 mt-2">Client Family Details</h3>
                                        {{-- <span class="mb-10 form-text text-muted">Want to edit Family Detail <a data-wizard-type="step" data-wizard-state="current" href="#"><u>Click here</u></a>.</span> --}}
                                        </div>
                                        <div class="row mt-md-5 mt-2">
                                            <div class="col-md-4">
                                                <img height="100px" src="{{asset('img/family.PNG')}}">
                                            </div>
                                            <div class="col-md-8">
                                                <div style="text-align: left" class="mt-4 ml-2">
                                                <ul>
                                                    <li><label id="r_name"></label></li>
                                                    <li><label id="r_dob"></label></li>
                                                </ul>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mt-10">
                                <div class="row mt-10">
                                    <div class="mt-10 col-md-12 col-sm-12">
                                        <h2 class="mt-10 font-weight-bold text-dark">List of documents required by client</h2>
                                        <span class="form-text text-muted">As the client has choosen the following income types so he needs to submit the following documents.</span>
                                        {{-- <span class="mb-10 form-text text-muted">Want to edit income type <a data-wizard-type="step" href="#"><u>Click here</u></a>.</span> --}}
                                    </div>
                                    <div class="col-md-12 mt-10 example-preview custom-set-main">
                                        <table id="tblIncome" class="table mb-5 table-striped custom-set">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col">Income type</th>
                                                    <th scope="col">Country</th>
                                                    <th scope="col">Form Required</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              @foreach($txtProfileDocList as $tpd)
                                              <tr id="tblRow<?= $tpd->income_id.'-'.$tpd->country_id ?>">
                                                <td>{{ $tpd->income_name }}</td>
                                                <td>{{ $tpd->country_name }}</td>
                                                <td>
                                                  <ul>
                                                    <?php $ind = 0; ?>
                                                    @foreach($tpd->docx as $doc)
                                                      <?php $name = $tpd->income_id.'-'.$tpd->country_id.'input[]';
                                                          if($tpd->country_id == '-1'){ 
                                                             $name = 'extraField[]';
                                                         } $ind++; ?>
                                                      <li id="inpDox<?= $ind.$tpd->income_id.'-'.$tpd->country_id ?>"><div class="mb-1 input-group">
                                                        <input class="form-control" value="{{$doc->docx}}" name="<?= $name ?>" type="text" >
                                                        <div class="input-group-append">
                                                          <a class="delete-repeater my-2" onclick="removeField('<?= $ind.$tpd->income_id.'-'.$tpd->country_id ?>')" class="btn btn-outline-primary" type="button">
                                                          <b>X</b></a>
                                                        </div>
                                                       </li>
                                                    @endforeach
                                                  </ul>
                                                </td>
                                              </tr>
                                              @endforeach
                                            </tbody>
                                        </table>
                                        <a onclick="addMoreFiled()" style="cursor:pointer" class="float-right"><u>Add More</u></a><br>
                                        <button hidden type="submit" value="pdf" name="submit" style="float: right" class="btn mt-10 btn-sm btn-secondary">Download Pdf</button>
                                      <div class="mt-10 mt-10 form-group">
                                        <label for="exampleTextarea">Notes..</label>
                                        <textarea name="profile_notes" class="form-control form-control-solid" rows="3"><?=$client->notes ?></textarea>
										                  </div>
                                    </div>  
                                </div>

                                            <button type="submit" class="float-right mt-10 btn btn-success edit-saveChanges2">
                                              Save Changes
                                            </button>
                            </div>
                          <script>
                             function removeField(id){
                              $('#inpDox'+id).remove();
                            }
                          
                          </script>
                          
                          <script>
                            var newRow = 1;
                            function addMoreFiled(){
                              var docList = '<ul>';
                              docList += '<li><div class="mb-1 input-group"><input name="extraField[]" class="form-control" type="text">'+
                                    '<div class="input-group-append">'+
                                    '<a class="delete-repeater my-2" onclick="removeExtraField('+newRow+')" name="extraField[]" class="btn btn-outline-primary" type="button">'+
                                     '<b>X</b></a>'+
                                  '</div><div></li></ul>';
                              $('#tblIncome').append('<tr colspan="3" id="newFieldRow'+newRow+'"><td></td><td></td><td>'+docList+'</td></tr>');
                            }
                            function removeExtraField(id){
                              $('#newFieldRow'+id).remove();
                            }
                          </script>
                            <!--end: Wizard Step 3-->
                            <!--begin: Wizard Actions-->
                            <div class="d-flex justify-content-between pt-7">
                                <div class="mr-2">
                                    <button type="button" onclick="changeFlag()" class="btn btn-light-primary font-weight-bolder font-size-h6 pr-8 pl-6 py-4 my-3 mr-3" data-wizard-type="action-prev">
                                    <span class="svg-icon svg-icon-md mr-2">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Left-2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <rect fill="#000000" opacity="0.3" transform="translate(15.000000, 12.000000) scale(-1, 1) rotate(-90.000000) translate(-15.000000, -12.000000)" x="14" y="7" width="2" height="10" rx="1"></rect>
                                                <path d="M3.7071045,15.7071045 C3.3165802,16.0976288 2.68341522,16.0976288 2.29289093,15.7071045 C1.90236664,15.3165802 1.90236664,14.6834152 2.29289093,14.2928909 L8.29289093,8.29289093 C8.67146987,7.914312 9.28105631,7.90106637 9.67572234,8.26284357 L15.6757223,13.7628436 C16.0828413,14.136036 16.1103443,14.7686034 15.7371519,15.1757223 C15.3639594,15.5828413 14.7313921,15.6103443 14.3242731,15.2371519 L9.03007346,10.3841355 L3.7071045,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(9.000001, 11.999997) scale(-1, -1) rotate(90.000000) translate(-9.000001, -11.999997)"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>Previous</button>
                                </div>
                                <div>
                                    <button hidden name="submit" value="save" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-submit" type="submit" id="kt_login_signup_form_submit_button">Submit 
                                    <span class="svg-icon svg-icon-md ml-2">
                                        <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Right-2.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
                                                <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span></button>
                                    <button type="button" onclick="setCurrentData()" class="btn btn-primary font-weight-bolder font-size-h6 pl-8 pr-4 py-4 my-3" data-wizard-type="action-next">Next 
                                    <span class="svg-icon svg-icon-md ml-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <rect fill="#000000" opacity="0.3" transform="translate(8.500000, 12.000000) rotate(-90.000000) translate(-8.500000, -12.000000)" x="7.5" y="7.5" width="2" height="9" rx="1"></rect>
                                                <path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span></button>
                                </div>
                            </div>
                          
                          <div class="modal fade" id="warningModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Warning!</h5><br>
                                    </div>
                                    <div class="modal-body">
                                      <p class="modal-title" id="exampleModalLabel">If you make any changes on this page, Your list of documents will be regenerated.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-primary font-weight-bold" onclick="submitForm()" data-dismiss="modal">ok</button>
                                    </div>
                                </div>
                            </div>
                          </div>
                            <!--end: Wizard Actions-->
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Wizard 6-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>
    <!--end::Container-->
</div>


@endsection


<script>
  var flag = 0;
  function changeFlag(){
    flag = flag-1;
  }
    window.addEventListener('load', function () {
      <?php if($client->msSingle == 1) {?>
        $('#spouse_div').hide();
        $('#family_div').hide();
      <?php } ?>
  //     $('#citizenship').select2();
    });
  function submitForm(){
    console.log('submit');
//     location.reload();
//     $('#kt_wizard_form').submit();
  }
    function setCurrentData(){
      flag = flag+1;
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var dob = $('#dob').val();
        var city = $('#city').val();
        $('#m_name').html(fname+" "+ lname);
        $('#m_dob').html(dob);
        $('#m_relation').html(city);

        var sfname = $('#sfname').val();
        var slname = $('#slname').val();
        var sdob = $('#sdob').val();
        $('#r_name').html(sfname+" "+slname);
        $('#r_dob').html(sdob);
      if(flag == 1){
        $('#warningModal').modal('show');
      }
        // $('#r_relation').html(city);        

    }

</script>