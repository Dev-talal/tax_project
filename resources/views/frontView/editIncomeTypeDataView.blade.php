@extends('frontView.masterView') @section('main_body')
<style>
  .croppie-container {
    padding: 0px;
  }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
  <div class="container">
    <div class="card card-custom">
      <div class="card-body p-0">
        <!--begin::Wizard-->
        <div class="wizard wizard-1" id="kt_wizard_v1" data-wizard-state="first" data-wizard-clickable="false">
          <!--begin::Wizard Nav-->
          <!--end::Wizard Nav-->
          <!--begin::Wizard Body-->
          <div class="row my-10 px-8 my-lg-15 px-lg-10">
            <div class="col-xl-12 col-xxl-12">
              <!--begin::Wizard Form-->
              <form method="post" action="/editIncomeDataSave" class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_form" enctype="multipart/form-data">
                <!--begin::Wizard Step 1-->
                {{ csrf_field() }}
                <div class="row">
                  <div class="col-md-12 offset-3 col-sm-12">
                    <h3 class="font-weight-bold text-dark">Edit Income Type</h3>
                    <div class="mb-10 fv-plugins-message-container">Edit Income Type </div>
                  </div>
                  <input type="hidden" name="img64" id="img64">
                  <div class="text-center col-md-6 col-sm-12">
                    <div id="upload-demo" style="padding=0; margin:0;"></div>

                    <!--                                         <div  style=" margin-top: 20%;">
                                            <img id="blah" src="{{asset('img/person.PNG')}}" width="200px" class="rounded-circle"><br>
                                        </div> -->
                    <input class="mt-2 mb-5" required onchange="readURL(this)" type='file' name="image" id="upload" /> {{-- <img id="blah" src="#" alt="your image" /> --}}
                  </div>
                  <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
                  <script src="http://demo.itsolutionstuff.com/plugin/croppie.js"></script>

                  <script>
                    $uploadCrop = $('#upload-demo').croppie({
                      enableExif: true,
                      viewport: {
                        width: 200,
                        height: 200,
                        type: 'circle'
                      },
                      boundary: {
                        width: 250,
                        height: 250
                      },url: '{{ url("storage/app/".$incomeTypeData->img_url)}}'
                    });

                    $('#upload').on('change', function() {
                      var reader = new FileReader();
                      reader.onload = function(e) {
                        $uploadCrop.croppie('bind', {
                          url: e.target.result
                        }).then(function() {
                          console.log('jQuery bind complete');
                        });
                      }
                      reader.readAsDataURL(this.files[0]);
                    });

                    function readURL(input) {
                      if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function(e) {
                          $('#blah').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                      }
                    }
                  </script>
                  <div class="col-md-6 col-sm-12">
                    <div>
                      <!--begin::Input-->
                      <div class="form-group fv-plugins-icon-container">
                        <label>Income Type Category</label>
                        <select required name="incomeTypeCategory" id="incomeTypeCategory" class="form-control form-control-solid form-control-lg">
                                                    <option value="">Select Incom Category</option>
                                                    @foreach ($incomeTypeList as $incom)
                                                    <option <?php if($incom->income_type_id == $incomeTypeData->income_type_id) echo'selected'; ?>  value="<?= $incom->income_type_id ?>"><?= $incom->income_type_name ?></option>
                                                        
                                                    @endforeach
                                                </select>
                      </div>
                    </div>
                    <div>
                      <!--begin::Input-->
                      <div class="form-group fv-plugins-icon-container">
                        <label>Income Type Name</label>
                        <input value="<?= $incomeTypeData->name ?>" type="text" required name="incomeType" id="incomeType" class="form-control form-control-solid form-control-lg" placeholder="Income Type Name">
                      </div>
                    </div>
                    <div>
                      <!--begin::Input-->
                      <div class="form-group fv-plugins-icon-container">
                        <label>Form Type</label>
                        <select name="formType" id="formType" required class="form-control form-control-solid form-control-lg">
                                                    <option value="">Select Form type</option>
                                                    @foreach ($formTypeList as $form)
                                                    <option <?php if($form->form_type_id == $incomeTypeData->form_type) echo'selected'; ?> value="<?= $form->form_type_id ?>"><?= $form->form_type_name ?></option>
                                                        
                                                    @endforeach
                                                </select>
                      </div>
                    </div>
                    <div>
                      <!--begin::Input-->
                      <div class="form-group fv-plugins-icon-container">
                        <label>Minimum Fee</label>
                        <input value="<?= $incomeTypeData->min_fee ?>" type="number" required name="minFee" id="minFee" class="form-control form-control-solid form-control-lg" placeholder="e.g 10">
                      </div>
                    </div>
                    <div>
                      <!--begin::Input-->
                      <div class="form-group fv-plugins-icon-container">
                        <label>Maximum Fee</label>
                        <input value="<?= $incomeTypeData->max_fee ?>" type="number" required name="maxFee" id="maxFee" class="form-control form-control-solid form-control-lg" placeholder="e.g 120">
                      </div>
                    </div>
                  </div>
                </div>
                <hr>

                <div class="row">
                  <div class="col-md-12 col-sm-12">
                    <h3 class="font-weight-bold text-dark">Documnet required by country:</h3>
                    <div class="mb-10 fv-plugins-message-container">Write down the documents required by each country, in the relevant country field</div>
                  </div>
                  <div id="country_div" style="margin: 0px; padding:0" class="col-md-12 col-sm-12 row">
                    <?php $flag= true; $docxCounter = 0; $counter = 0; 
                                                foreach($incomeTypeDocx as $itd){ 
                                                  $counter++;
                                                   $docFlag = true; ?>
                    <div style="padding:0!important; margin: 0px" class="col-md-12 col-sm-12 row" id="country<?= $counter ?>">
                      <div class="col-md-12">
                        <?php if($counter != 1){ ?>
                        <a class="delete-repeater mt-3" style="cursor:pointer" onclick="removeCoutryDiv('<?= $counter ?>')" class="float-right"><b>X</b></a>
                        <?php } ?>
                      </div>
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group fv-plugins-icon-container">
                          <label>Country</label>
                          <select name="country[]" required class="form-control form-control-solid form-control-lg">
                                                        <option value="">Select Country</option>
                                                        @foreach ($countryList as $country)
                                                        <option <?php if($country->country_id == $itd->country_id) echo'selected'; ?> value="<?= $country->country_id ?>"><?= $country->country_name ?></option>
                                                            
                                                        @endforeach

                                                    </select>
                        </div>
                      </div>
                      <div id="docx_div<?= $counter ?>" class="col-md-6 col-sm-12">
                        <label>Document Needed</label>
                        <?php foreach($itd->docx as $d){ $docxCounter++; ?>
                        <div id="docx<?= $docxCounter ?>" class="mb-5 input-group">
                          <input value="<?= $d->docs ?>" type="text" class="form-control form-control-solid" name="<?= $counter ?>ducuments[]" id="name" placeholder="E.g statement of investement income">
                          <div class="input-group-append">
                            <?php if($docFlag == true){ ?>
                            <a onclick="add_more_docx(<?= $counter ?>)" class="btn btn-outline-primary" type="button">
                                                  <b>+</b>
                                                </a>
                            <?php } else{ ?>
                            <a onclick="remove_more_docx(<?= $counter ?>)" class="delete-repeater mt-3" type="button">
                                                  <b style="padding:10px 15px;">X</b>
                                                </a>
                            <?php } ?>
                          </div>
                        </div>
                        <?php $docFlag = false; } ?>
                      </div>
                      <?php  } ?>
                    </div>
                    <input type="hidden" name="incomeId" value="<?= $id ?>"
                    <input type="hidden" id="docxCounter" name="docxCounter" value="<?= $docxCounter ?>" />
                    <input type="hidden" name="countryCounter" id="countryCounter" value="<?= $counter ?>" />
                  </div>
                  
                </div>
                  <div class="col-md-12 mt-3">
                    <a style="cursor:pointer" onclick="addmore()" class="float-right"><u>Add More</u></a>
                  </div>
                <div class="d-flex justify-content-between mt-5 pt-10">
                  <div class="mr-2">
                    <input type="hidden" name="addMoreDocx" value="[-1]" id="addMoreDocx">
                  </div>
                  <div>
                    <a id="upload-result" type="submit" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">Save Changes</a>
                  </div>
                </div>
              </form>

            </div>
          </div>
          <!--end::Wizard Body-->
        </div>

        <script>
          $('#upload-result').on('click', function(ev) {
            $uploadCrop.croppie('result', {
              type: 'canvas',
              size: 'viewport'
            }).then(function(resp) {
              console.log(resp);
              $('#img64').val(resp);
              if ($('#incomeTypeCategory').val() == '') {
                $('#incomeTypeCategory').after('<p class="text-danger">Income Type Category is required.</p>');
                return;
              }
              if ($('#incomeType').val() == '') {
                $('#incomeType').after('<p class="text-danger">Income Type Name is required.</p>');
                return;
              }
              if ($('#formType').val() == '') {
                $('#formType').after('<p class="text-danger">Form Type is required.</p>');
                return;
              }
              if ($('#minFee').val() == '') {
                $('#minFee').after('<p class="text-danger">Minimum Fee is required.</p>');
                return;
              }
              if ($('#maxFee').val() == '') {
                $('#maxFee').after('<p class="text-danger">Maximum Fee is required.</p>');
                return;
              }
              $("#kt_form").submit();
              return;
            });
          });

          window.addEventListener('load', function() {});

          function remove_more_docx(id) {
            $('#docx' + id).remove();
            //              docsDiv--;
          }

          function add_more_docx(id = '-1') {
            var docsDiv = $('#docxCounter').val();
            console.log(docsDiv);
            docsDiv++;
            $('#docxCounter').val(docsDiv);
            var d = 'docx_div';
            if (id != '-1')
              d = 'docx_div' + id;
            $('#' + d).append('<div id="docx' + docsDiv + '" class=" my-5 input-group">' +
              '<input type="text" class="form-control form-control-solid" name="' + id + 'ducuments[]" id="name" placeholder="E.g statement of investement income">' +
              '<div class="input-group-append">' +
              '<a onclick="remove_more_docx(' + docsDiv + ')" class="delete-repeater mt-3" type="button">' +
              '<b style="padding:10px 15px;">X</b>' +
              '</a>' +
              '</div>' +
              '</div>');
          }

          var countryDiv = 0;

          function removeCoutryDiv(id) {

            //              var val = $('#addMoreDocx').val();
            //              val = JSON.parse(val);
            //              val = removeItem(val, countryDiv);
            //              $('#addMoreDocx').val(JSON.stringify(val));
            console.log("hello");
            countryDiv = $('#countryCounter').val();
            $('#country' + countryDiv).remove();
            countryDiv--;
            $('#countryCounter').val(countryDiv);
          }

          function removeItem(arr, item) {
            return arr.filter(f => f !== item)
          }

          function addmore() {
            countryDiv = $('#countryCounter').val();
            countryDiv++;
            $('#countryCounter').val(countryDiv);
            var val = $('#addMoreDocx').val();
            val = JSON.parse(val);
            val.push(countryDiv);
            console.log(val);
            $('#addMoreDocx').val(JSON.stringify(val));

            $('#country_div').append('<div style="padding:0!important; margin: 0px" class="col-md-12 col-sm-12 row" id="country' + countryDiv + '">' +
              '<div class="col-md-12">' +
              '<a class="delete-repeater mt-3" style="cursor:pointer" onclick="removeCoutryDiv(' + countryDiv + ')" class="float-right" ><b>X</b></a>' +
              '</div>' +
              '<div class="col-md-6 col-sm-12">' +
              '<div class="form-group fv-plugins-icon-container">' +
              '<label>Country</label>' +
              '<select name="country[]" required class="form-control form-control-solid form-control-lg">' +
              @foreach($countryList as $country)
              '<option value="<?= $country->country_id ?>"><?= $country->country_name ?></option>' +

              @endforeach

              '</select>' +
              '</div>' +
              '</div>' +
              '<div id="docx_div' + countryDiv + '" class="col-md-6 col-sm-12">' +
              '<label>Document Needed</label>' +
              '<div class="input-group">' +
              '<input type="text" class="form-control form-control-solid" name="' + countryDiv + 'ducuments[]" id="name" placeholder="E.g statement of investement income">' +
              '<div class="input-group-append">' +
              '<a onclick="add_more_docx(' + countryDiv + ')" class="btn btn-outline-primary"><b>+</b></a>' +
              '</div>' +
              '</div>' +
              '</div></div>');
          }
        </script>

        <!--end::Wizard-->
      </div>
      <!--end::Wizard-->
    </div>
  </div>




</div>
@endsection