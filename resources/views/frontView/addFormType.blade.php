@extends('frontView.masterView')

@section('main_body')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard-->
                <div class="wizard wizard-1" id="kt_wizard_v1" data-wizard-state="first" data-wizard-clickable="false">
                    <!--begin::Wizard Nav-->
                    <!--end::Wizard Nav-->
                    <!--begin::Wizard Body-->
                    
                    <div class="row justify-content-center my-10 px-8 my-lg-15 px-lg-10">
                        <div class="col-xl-12 col-xxl-7">
                            @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                            @endif
<<<<<<< HEAD
=======
                          @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                            @endif
>>>>>>> build 2
                            <!--begin::Wizard Form-->
                            <form method="post" class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_form" action="addFormType">
                                <!--begin::Wizard Step 1-->
                                {{ csrf_field() }}

                                <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                    <h3 class="mb-10 font-weight-bold text-dark">Form type</h3>
                                    <!--begin::Input-->
                                    <div class="form-group fv-plugins-icon-container">
                                        <label>Form type</label>
                                        <input type="text" required class="form-control form-control-solid form-control-lg" name="name" id="name" placeholder="Form type">
                                    <div class="fv-plugins-message-container"></div></div>
                                </div>
                                <div class="d-flex justify-content-between mt-5 pt-10">
                                    <div class="mr-2">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">Save</button>
                                    </div>
                                </div>
                                <!--end::Wizard Actions-->
                            <div></div><div></div><div></div><div></div></form>
                            <!--end::Wizard Form-->
                        </div>
                    </div>
                    <!--end::Wizard Body-->
                </div>

         

                <!--end::Wizard-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>




</div>
@endsection