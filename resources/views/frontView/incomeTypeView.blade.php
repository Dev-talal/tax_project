@extends('frontView.masterView')

@section('main_body')

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <h5 class="pt-5 ml-2">Income Type Category</h5><br>
                <div class="d-flex justify-content-between pt-5  pr-10">
                    <div class="mr-2">
                    <input class="ml-2 form-control" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search by type.." title="Type in a name">
                    </div>
                    <div>
                        <?php if(json_decode(Session::get('user')->role_data)->addIncomTypeCat == 1){ ?>
                            <a href="{{url('/addIncomeType')}}" class="btn btn-success font-weight-bold text-uppercase px-9 py-4">Add New</a>
                   <?php } ?>
                    </div>
                </div>
                @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif
                <div class="example-preview">
                    <table class="table mb-5 table-striped" id="datatable">
                        <thead class="thead">
                            <tr>
                                <th scope="col">Date of build</th>
                                <th scope="col">Income Type Category Name</th>
                                {{-- <th scope="col">Status</th> --}}
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $count = 0;
                         foreach($income_type_list as $List){
                            $count++;
                            
                            ?>
                            <tr>
                                <td scope="col">
                                <?php echo $List ->created_at ?>
                                </td>
                                <td>
                                <?php echo $List ->income_type_name ?>
                                </td>
                                <td class="text-left pr-0">
									<?php if(json_decode(Session::get('user')->role_data)->editIncomTypeCat == 1){ ?>
                               								
								<a href="#" onclick="editData('<?php echo $List->income_type_id ?>','<?php echo $List->income_type_name ?>')" data-toggle="modal" data-target="#editModal" class="btn btn-icon btn-light btn-hover-primary btn-sm">
								<span class="svg-icon svg-icon-md svg-icon-primary">
																			<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Write.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24"></rect>
																					<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)"></path>
																					<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
																				</g>
																			</svg>
																			<!--end::Svg Icon-->
																		</span>
																	</a>
                                                                    <?php } ?>
                                                                    <?php if(json_decode(Session::get('user')->role_data)->deleteIncomTypeCat == 1){ ?>
																	<a  href="deleteIncomeType/<?php echo $List ->income_type_id ?>"  class="btn btn-icon btn-light btn-hover-primary btn-sm">
																		<span class="svg-icon svg-icon-md svg-icon-primary">
																			<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Trash.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24"></rect>
																					<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
																					<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
																				</g>
																			</svg>
																			<!--end::Svg Icon-->
																		</span>
                                                                    </a>
                                                                <?php } ?>
																</td>
                            </tr>
                            <?php 
                        }
                        ?>
                            <!-- <tr>
                                <th scope="col">2</th>
                                <td>Ana</td>
                                <td>Jacobs</td>
                                <td>
                                    <span class="label label-inline label-light-success font-weight-bold">Approved</span>
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">3</th>
                                <td>Larry</td>
                                <td>Pettis</td>
                                <td>
                                    <span class="label label-inline label-light-danger font-weight-bold">New</span>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                    
                </div>
            

            </div>
            <!--end::Wizard-->
        </div>
    </div>
</div>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
   




</div>
<!-- Modal-->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit income type category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    X
                </button>
            </div>
            <form method="post" class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_form" action="updateIncomeCategory">
             {{csrf_field()}}
            <div class="modal-body">
                <input type="hidden" name="income_category_id" id="income_category_id">
            <div class="form-group fv-plugins-icon-container">
                                        <label>Income Category Name</label>
                                        <input type="text" class="form-control form-control-solid form-control-lg" name="income_category_name" id="income_category_name" placeholder="Income Category Name">
                                    <div class="fv-plugins-message-container"></div></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
window.addEventListener('load', function() {
    $('#datatable').DataTable();
    $('#datatable_filter').hide();
    $('#datatable_length').hide();
  $('#datatable_info').addClass("m-5");
    $('#datatable_paginate').addClass("m-5");
    });

function editData(id, name){
    $('#income_category_id').val(id);
    $('#income_category_name').val(name);
    // ('#editModal').modal('show');
}
  function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("datatable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
@endsection