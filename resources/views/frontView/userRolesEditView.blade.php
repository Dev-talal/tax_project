@extends('frontView.masterView')

@section('main_body')
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Wizard-->
                <div class="wizard wizard-1" id="kt_wizard_v1" data-wizard-state="first" data-wizard-clickable="false">
                    <!--begin::Wizard Nav-->
                    <!--end::Wizard Nav-->
                    <!--begin::Wizard Body-->
                    <div class="row my-10 px-8 my-lg-15 px-lg-10">
                        <div class="col-xl-12 col-xxl-12">
                            <!--begin::Wizard Form-->
                            <form method="post" class="form fv-plugins-bootstrap fv-plugins-framework" id="kt_form" action="/editRoleSave">
                                <!--begin::Wizard Step 1-->
                                {{ csrf_field() }}

                                <div class="col-md-6 pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                    <h3 class="font-weight-bold text-dark">Edit Roles</h3>
                                    <div class="mb-10  fv-plugins-message-container">Please Edit roles here.</div>
                                    <!--begin::Input-->
                                    <div class="form-group fv-plugins-icon-container">
                                        <label>Role Name</label>
                                        <input value="{{$role->name}}" type="text" class="form-control form-control-solid form-control-lg" name="name" id="name" placeholder="Role Name">
                                    <div class="fv-plugins-message-container"></div></div>
                                </div>
                              <input type="hidden" name="updateId" value={{$id}}>
                              <?php $roleJson = json_decode($role->role_data);?>
                                <div class="row">
                                    <div class="col-md-4 mt-10 col-sm-12">
                                        <label><b>Client</b></label>
                                        <label class="checkbox mt-2 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->addClient == 1) echo 'checked'; ?> type="checkbox" name="addClient" >
                                            <span></span>&nbsp;&nbsp; Can Add Client
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->editClient == 1) echo 'checked'; ?> type="checkbox" name="editClient" >
                                            <span></span>&nbsp;&nbsp; Can Edit Client
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->deleteClient == 1) echo 'checked'; ?> type="checkbox" name="deleteClient" >
                                            <span></span>&nbsp;&nbsp; Can Delete Client
                                        </label>
                                        <label hidden class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input type="checkbox" name="updateClient" >
                                            <span></span>&nbsp;&nbsp; Can update Client file
                                        </label>

                                    </div>
                                    <div class="col-md-4 mt-10 col-sm-12">
                                        <label><b>Roles</b></label>
                                        <label class="checkbox mt-2 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->addRole == 1) echo 'checked'; ?> type="checkbox" name="addRole" >
                                            <span></span>&nbsp;&nbsp; Can Add New Roles
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->editRole == 1) echo 'checked'; ?> type="checkbox" name="editRole" >
                                            <span></span>&nbsp;&nbsp; Can Edit Roles
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->deleteRole == 1) echo 'checked'; ?> type="checkbox" name="deleteRole" >
                                            <span></span>&nbsp;&nbsp; Can Delete Roles
                                        </label>
                                    </div>
                                    <div class="col-md-4 mt-10 col-sm-12">
                                        <label><b>Form Type</b></label>
                                        <label class="checkbox mt-2 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->addForm == 1) echo 'checked'; ?> type="checkbox" name="addForm" >
                                            <span></span>&nbsp;&nbsp; Can Add Form Type
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->editForm == 1) echo 'checked'; ?> type="checkbox" name="editForm" >
                                            <span></span>&nbsp;&nbsp; Can Edit Form Type
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->deleteForm == 1) echo 'checked'; ?> type="checkbox" name="deleteForm" >
                                            <span></span>&nbsp;&nbsp; Can Delete Form Type
                                        </label>
                                        
                                    </div>
                                </div>


                                <div class=" row">
                                    <div class="col-md-4 mt-10 col-sm-12">
                                        <label><b>Admin User</b></label>
                                        <label class="checkbox mt-2 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->addAdmin == 1) echo 'checked'; ?> type="checkbox" name="addAdmin" >
                                            <span></span>&nbsp;&nbsp; Can Add Admin User
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->editAdmin == 1) echo 'checked'; ?> type="checkbox" name="editAdmin" >
                                            <span></span>&nbsp;&nbsp; Can Edit Admin User
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->deleteAdmin == 1) echo 'checked'; ?> type="checkbox" name="deleteAdmin" >
                                            <span></span>&nbsp;&nbsp; Can Delete Admin User
                                        </label>
                                    </div>
                                    <div class="col-md-4 mt-10 col-sm-12">
                                        <label><b>Income Type</b></label>
                                        <label class="checkbox mt-2 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->addIncomType == 1) echo 'checked'; ?> type="checkbox" name="addIncomType" >
                                            <span></span>&nbsp;&nbsp; Can Add New Income Type
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->editIncomType == 1) echo 'checked'; ?> type="checkbox" name="editIncomType" >
                                            <span></span>&nbsp;&nbsp; Can Edit Income Type
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->deleteIncomType == 1) echo 'checked'; ?> type="checkbox" name="deleteIncomType" >
                                            <span></span>&nbsp;&nbsp; Can Delete Income Type
                                        </label>
                                    </div>
                                  <div class="col-md-4 mt-10 col-sm-12">
                                        <label><b>Income Type category</b></label>
                                        <label class="checkbox mt-2 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->addIncomTypeCat == 1) echo 'checked'; ?> type="checkbox" name="addIncomTypeCat" >
                                            <span></span>&nbsp;&nbsp; Can Add New Income Type category
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->editIncomTypeCat == 1) echo 'checked'; ?> type="checkbox" name="editIncomTypeCat" >
                                            <span></span>&nbsp;&nbsp; Can Edit Income Type category
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->deleteIncomTypeCat == 1) echo 'checked'; ?> type="checkbox" name="deleteIncomTypeCat" >
                                            <span></span>&nbsp;&nbsp; Can Delete Income Type category
                                        </label>
                                    </div>
                                </div>
                              <div class="mt-10 row">
                                <div class="col-md-4 col-sm-12">
                                        <label><b>Country</b></label>
                                        <label class="checkbox mt-2 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->addCountry == 1) echo 'checked'; ?> type="checkbox" name="addCountry" >
                                            <span></span>&nbsp;&nbsp; Can Add Country
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->editCountry == 1) echo 'checked'; ?> type="checkbox" name="editCountry" >
                                            <span></span>&nbsp;&nbsp; Can Edit Country
                                        </label>
                                        <label class="checkbox mt-5 checkbox-outline checkbox-outline-2x checkbox-primary">
                                            <input <?php if($roleJson->deleteCountry == 1) echo 'checked'; ?> type="checkbox" name="deleteCountry" >
                                            <span></span>&nbsp;&nbsp; Can Delete Country
                                        </label>
                                        
                                    </div>
                              </div>
                                <div class="d-flex justify-content-between mt-5 pt-10">
                                    <div class="mr-2">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary font-weight-bold text-uppercase px-9 py-4" data-wizard-type="action-next">Save Changes</button>
                                    </div>
                                </div>
                                <!--end::Wizard Actions-->
                            <div></div><div></div><div></div><div></div></form>
                            <!--end::Wizard Form-->
                        </div>
                    </div>
                    <!--end::Wizard Body-->
                </div>

         

                <!--end::Wizard-->
            </div>
            <!--end::Wizard-->
        </div>
    </div>




</div>
@endsection