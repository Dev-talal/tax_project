-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 16, 2020 at 01:10 AM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tax_profile`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(200) NOT NULL,
  `created_at` date NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `created_at`, `status`) VALUES
(23, 'United States', '2020-08-10', 1),
(24, 'Australia', '2020-08-10', 1),
(25, 'United Kingdom', '2020-08-10', 1),
(26, 'New Zealand', '2020-08-10', 1),
(27, 'Canada', '2020-08-10', 1),
(29, 'Country 2', '2020-08-25', 1),
(33, 'Country 3', '2020-08-25', 1),
(35, 'Country 9', '2020-08-25', 1),
(36, 'Country 66', '2020-08-25', 1),
(38, 'Country 41', '2020-08-25', 1),
(39, 'Country 30', '2020-08-25', 1),
(40, 'Country 39', '2020-08-25', 1),
(41, 'Country 367', '2020-08-25', 1),
(42, 'Country 398', '2020-08-25', 1),
(43, 'Country 356', '2020-08-25', 1),
(44, 'Country 311', '2020-08-25', 1),
(46, 'Kim Waters', '2020-08-27', 1),
(47, 'Kaseem Noble', '2020-08-27', 1),
(48, 'Winifred Franks', '2020-08-27', 1),
(49, 'Chastity Mack', '2020-08-27', 1),
(56, 'pak', '2020-09-11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `form_type`
--

CREATE TABLE `form_type` (
  `form_type_id` int(11) NOT NULL,
  `form_type_name` varchar(200) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `form_type`
--

INSERT INTO `form_type` (`form_type_id`, `form_type_name`, `created_at`) VALUES
(7, 'Form 1040 (Line 1); Form 2555', '2020-08-10'),
(8, 'Schedule B; Form 1040 Line 2b', '2020-08-10'),
(9, 'Schedule B; Form 1040 Line 3', '2020-08-10'),
(10, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(11, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(12, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(13, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(14, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(15, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(16, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(17, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(18, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(19, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(20, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(21, 'Form 1040 (Line 1); Form 2555', '2020-08-25'),
(22, 'Judah Neal', '2020-08-27'),
(23, 'Hiroko Andrews', '2020-08-28'),
(24, 'Oqydy', '2020-08-31'),
(28, 'Salary', '2020-09-11'),
(29, 'hello', '2020-09-11');

-- --------------------------------------------------------

--
-- Table structure for table `income_type`
--

CREATE TABLE `income_type` (
  `id` int(11) NOT NULL,
  `name` text,
  `income_type_id` int(11) DEFAULT NULL,
  `form_type` int(11) DEFAULT NULL,
  `min_fee` double DEFAULT NULL,
  `max_fee` double DEFAULT NULL,
  `country_id` text,
  `documents` text,
  `img_url` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income_type`
--

INSERT INTO `income_type` (`id`, `name`, `income_type_id`, `form_type`, `min_fee`, `max_fee`, `country_id`, `documents`, `img_url`) VALUES
(24, 'Test income type', 14, 7, 10, NULL, NULL, NULL, '1598349013.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `income_type_category_name`
--

CREATE TABLE `income_type_category_name` (
  `income_type_id` int(11) NOT NULL,
  `income_type_name` varchar(200) NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `income_type_category_name`
--

INSERT INTO `income_type_category_name` (`income_type_id`, `income_type_name`, `created_at`) VALUES
(14, 'Income Type', '2020-08-10'),
(18, 'Deductions', '2020-08-10'),
(20, 'Taxes', '2020-08-10');

-- --------------------------------------------------------

--
-- Table structure for table `income_type_docx`
--

CREATE TABLE `income_type_docx` (
  `id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `docs` text,
  `income_type_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income_type_docx`
--

INSERT INTO `income_type_docx` (`id`, `country_id`, `docs`, `income_type_id`) VALUES
(1, 24, 'DVD', 21),
(2, 24, 'VCD', 21),
(3, 23, 'CNIC', 22),
(4, 23, 'Passport', 22),
(5, 24, 'A1', 22),
(6, 24, 'A2', 22),
(14, 23, 'Document required for US', 24),
(15, 23, '2nd document required for this', 24),
(16, 24, 'Document requires for australia', 24),
(17, 25, 'Document requored for UK', 24),
(19, 25, 'Required document accordingly', 26),
(39, 35, 'hyyuuyu', 39),
(32, 23, 'Dilocument requires', 32),
(33, 23, 'Perfect document', 32),
(37, 27, 'hello', 37);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` text,
  `role_data` text,
  `create_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `role_data`, `create_at`) VALUES
(1, 'Super Admin', '{\"addClient\":1,\"editClient\":1,\"deleteClient\":1,\"updateClient\":0,\"viewClient\":1,\"addRole\":1,\"editRole\":1,\"deleteRole\":1,\"viewRole\":1,\"addForm\":1,\"editForm\":1,\"deleteForm\":1,\"viewForm\":1,\"addAdmin\":1,\"editAdmin\":1,\"deleteAdmin\":1,\"viewAdmin\":1,\"addIncomType\":1,\"editIncomType\":1,\"deleteIncomType\":1,\"viewIncomType\":1,\"addIncomTypeCat\":1,\"editIncomTypeCat\":1,\"deleteIncomTypeCat\":1,\"viewIncomTypeCat\":1,\"addCountry\":1,\"editCountry\":1,\"deleteCountry\":1,\"viewCountry\":1}', '2020-09-07'),
(12, 'test role', '{\"addClient\":0,\"editClient\":0,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":0,\"addRole\":0,\"editRole\":0,\"deleteRole\":0,\"viewRole\":0,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":0,\"editCountry\":0,\"deleteCountry\":1,\"viewCountry\":1}', '2020-09-07'),
(13, 'user roles', '{\"addClient\":0,\"editClient\":1,\"deleteClient\":1,\"updateClient\":0,\"viewClient\":1,\"addRole\":1,\"editRole\":1,\"deleteRole\":1,\"viewRole\":1,\"addForm\":1,\"editForm\":1,\"deleteForm\":1,\"viewForm\":1,\"addAdmin\":1,\"editAdmin\":1,\"deleteAdmin\":1,\"viewAdmin\":1,\"addIncomType\":1,\"editIncomType\":1,\"deleteIncomType\":1,\"viewIncomType\":1,\"addIncomTypeCat\":1,\"editIncomTypeCat\":1,\"deleteIncomTypeCat\":1,\"viewIncomTypeCat\":1,\"addCountry\":1,\"editCountry\":1,\"deleteCountry\":1,\"viewCountry\":1}', '2020-09-05'),
(14, 'Test role', '{\"addClient\":0,\"editClient\":0,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":0,\"addRole\":0,\"editRole\":0,\"deleteRole\":0,\"viewRole\":0,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":0,\"editCountry\":0,\"deleteCountry\":0,\"viewCountry\":0}', '2020-09-07'),
(15, '123', '{\"addClient\":0,\"editClient\":0,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":0,\"addRole\":0,\"editRole\":0,\"deleteRole\":0,\"viewRole\":0,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":1,\"editCountry\":1,\"deleteCountry\":1,\"viewCountry\":1}', '2020-09-07'),
(16, 'Ok rolling', '{\"addClient\":1,\"editClient\":1,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":1,\"addRole\":1,\"editRole\":1,\"deleteRole\":0,\"viewRole\":1,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":0,\"editCountry\":0,\"deleteCountry\":1,\"viewCountry\":1}', '2020-08-25'),
(17, 'New rols', '{\"addClient\":1,\"editClient\":0,\"deleteClient\":1,\"updateClient\":0,\"viewClient\":1,\"addRole\":0,\"editRole\":1,\"deleteRole\":0,\"viewRole\":1,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":0,\"editCountry\":1,\"deleteCountry\":0,\"viewCountry\":1}', '2020-08-25'),
(18, 'Perfect role', '{\"addClient\":1,\"editClient\":1,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":1,\"addRole\":0,\"editRole\":0,\"deleteRole\":0,\"viewRole\":0,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":1,\"editCountry\":1,\"deleteCountry\":0,\"viewCountry\":1}', '2020-08-25'),
(19, 'Yo role', '{\"addClient\":1,\"editClient\":0,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":1,\"addRole\":1,\"editRole\":0,\"deleteRole\":0,\"viewRole\":1,\"addForm\":0,\"editForm\":1,\"deleteForm\":0,\"viewForm\":1,\"addAdmin\":1,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":1,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":0,\"editCountry\":1,\"deleteCountry\":0,\"viewCountry\":1}', '2020-08-25'),
(20, 'New roli', '{\"addClient\":1,\"editClient\":0,\"deleteClient\":1,\"updateClient\":0,\"viewClient\":1,\"addRole\":0,\"editRole\":0,\"deleteRole\":0,\"viewRole\":0,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":1,\"editCountry\":1,\"deleteCountry\":0,\"viewCountry\":1}', '2020-08-25'),
(21, 'User daya', '{\"addClient\":1,\"editClient\":0,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":1,\"addRole\":1,\"editRole\":0,\"deleteRole\":0,\"viewRole\":1,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":0,\"editCountry\":0,\"deleteCountry\":0,\"viewCountry\":0}', '2020-08-25'),
(22, 'Ok', '{\"addClient\":1,\"editClient\":0,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":1,\"addRole\":0,\"editRole\":0,\"deleteRole\":0,\"viewRole\":0,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":1,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":1,\"addIncomType\":1,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":1,\"addIncomTypeCat\":1,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":1,\"addCountry\":0,\"editCountry\":0,\"deleteCountry\":0,\"viewCountry\":0}', '2020-08-25'),
(23, 'Manager', '{\"addClient\":1,\"editClient\":1,\"deleteClient\":0,\"updateClient\":0,\"viewClient\":1,\"addRole\":0,\"editRole\":0,\"deleteRole\":0,\"viewRole\":0,\"addForm\":0,\"editForm\":0,\"deleteForm\":0,\"viewForm\":0,\"addAdmin\":0,\"editAdmin\":0,\"deleteAdmin\":0,\"viewAdmin\":0,\"addIncomType\":0,\"editIncomType\":0,\"deleteIncomType\":0,\"viewIncomType\":0,\"addIncomTypeCat\":0,\"editIncomTypeCat\":0,\"deleteIncomTypeCat\":0,\"viewIncomTypeCat\":0,\"addCountry\":0,\"editCountry\":0,\"deleteCountry\":0,\"viewCountry\":0}', '2020-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `tax_income_type`
--

CREATE TABLE `tax_income_type` (
  `id` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `income_id` int(11) DEFAULT NULL,
  `tax_profile_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_income_type`
--

INSERT INTO `tax_income_type` (`id`, `country_id`, `income_id`, `tax_profile_id`) VALUES
(7, 23, 24, 4),
(8, 24, 24, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tax_income_type_docx`
--

CREATE TABLE `tax_income_type_docx` (
  `id` int(11) NOT NULL,
  `docx` text,
  `tax_income_type_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_income_type_docx`
--

INSERT INTO `tax_income_type_docx` (`id`, `docx`, `tax_income_type_id`, `country_id`) VALUES
(11, 'Document requires for australia', 8, 24),
(9, 'Document required for US', 7, 23),
(10, '2nd document required for this', 7, 23);

-- --------------------------------------------------------

--
-- Table structure for table `tax_profile`
--

CREATE TABLE `tax_profile` (
  `id` int(11) NOT NULL,
  `fname` text,
  `lname` text,
  `dob` date DEFAULT NULL,
  `msSingle` int(11) DEFAULT NULL,
  `msMfj` int(11) DEFAULT NULL,
  `msMfs` int(11) DEFAULT NULL,
  `msHoh` int(11) DEFAULT NULL,
  `msQw` int(11) DEFAULT NULL,
  `sfname` text,
  `slname` text,
  `sdob` date DEFAULT NULL,
  `country` text,
  `city` text,
  `state` text,
  `address` text,
  `citizenship` text,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_profile`
--

INSERT INTO `tax_profile` (`id`, `fname`, `lname`, `dob`, `msSingle`, `msMfj`, `msMfs`, `msHoh`, `msQw`, `sfname`, `slname`, `sdob`, `country`, `city`, `state`, `address`, `citizenship`, `notes`) VALUES
(4, 'Will', 'Smith', '2020-01-28', 1, 0, 0, 0, 0, NULL, NULL, NULL, 'Australia', 'Har', 'Arizona', 'univeristy', '[\"Angola\"]', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tax_profile_depn`
--

CREATE TABLE `tax_profile_depn` (
  `id` int(11) NOT NULL,
  `fname` text,
  `lname` text,
  `relation` text,
  `child_tax_credit` int(11) DEFAULT NULL,
  `child_for_other` int(11) DEFAULT NULL,
  `tax_profile_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_profile_depn`
--

INSERT INTO `tax_profile_depn` (`id`, `fname`, `lname`, `relation`, `child_tax_credit`, `child_for_other`, `tax_profile_id`) VALUES
(4, NULL, NULL, NULL, 0, 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` text,
  `lname` text,
  `role` int(11) DEFAULT NULL,
  `email` text,
  `password` text,
  `image` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `role`, `email`, `password`, `image`) VALUES
(1, 'James Andreson', 'Simango', 1, 'ali@gmail.com', '1122', '1598892166.jpeg'),
(14, 'Talal', 'Tahir', 13, 'talal@mail.com', '22', NULL),
(15, 'Brent Best', 'Darius Nixon', 0, 'bixusyc@mailinator.net', 'Pa$$w0rd!', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `form_type`
--
ALTER TABLE `form_type`
  ADD PRIMARY KEY (`form_type_id`);

--
-- Indexes for table `income_type`
--
ALTER TABLE `income_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income_type_category_name`
--
ALTER TABLE `income_type_category_name`
  ADD PRIMARY KEY (`income_type_id`);

--
-- Indexes for table `income_type_docx`
--
ALTER TABLE `income_type_docx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_income_type`
--
ALTER TABLE `tax_income_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_income_type_docx`
--
ALTER TABLE `tax_income_type_docx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_profile`
--
ALTER TABLE `tax_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_profile_depn`
--
ALTER TABLE `tax_profile_depn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `form_type`
--
ALTER TABLE `form_type`
  MODIFY `form_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `income_type`
--
ALTER TABLE `income_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `income_type_category_name`
--
ALTER TABLE `income_type_category_name`
  MODIFY `income_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `income_type_docx`
--
ALTER TABLE `income_type_docx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tax_income_type`
--
ALTER TABLE `tax_income_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tax_income_type_docx`
--
ALTER TABLE `tax_income_type_docx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tax_profile`
--
ALTER TABLE `tax_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tax_profile_depn`
--
ALTER TABLE `tax_profile_depn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
