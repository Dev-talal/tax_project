<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncomeTypeDataModel extends Model{
    protected  $table = 'income_type';
    protected $fillable = [
        'name','income_type_id', 'form_type', 'min_fee', 'max_fee', 'country_id', 'documents', 'img_url'
    ];
    public $timestamps = false;
}
