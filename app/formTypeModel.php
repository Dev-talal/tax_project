<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class formTypeModel extends Model
{
    protected  $table = 'form_type';
    protected $fillable = [
        'form_type_name','created_at'
    ];
    public $timestamps = false;
}
