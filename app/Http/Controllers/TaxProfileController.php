<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;

class TaxProfileController extends Controller{
    
    public function addTaxProfile(){
        $incomeList =  DB::table('income_type')->get();
        $countryList =  DB::table('countries')
                        ->where('status', '=', 1)
                        ->select('*')
                         ->get();
        $allCountry = $this->getCountryList();
        $countryListIds = array();
        foreach($incomeList as $in){
            $country = DB::table('income_type_docx')->where('income_type_id', '=', $in->id)->groupBy('country_id')->get();
            $cnt = array();
            $docxArray = array();
            foreach($country as $con){
              array_push($countryListIds, $in->id.'-'.$con->country_id);
              array_push($cnt , DB::table('countries')->where('country_id', '=', $con->country_id)->first());
              $countryDocx = DB::table('income_type_docx')->where('income_type_id', '=', $in->id)
                ->where('country_id', '=', $con->country_id)->get();
              array_push($docxArray, json_encode($countryDocx));
            }
            $in->country = $cnt;
            $in->country_docx = $docxArray;
        }
//         echo '<pre>';
//         print_r($incomeList);die();
        return view('frontView.tax.addTaxProfile', compact('incomeList', 'countryListIds','countryList', 'allCountry'));
    }

    public function saveTaxProfile(Request $req){
        $fname = $req->fname;
        $lname = $req->lname;
        $dob = $req->dob;

        $msSingle = isset($req->chkSingle) ? 1 : 0;
        $msMfj = isset($req->chkMfj) ? 1 : 0;
        $msMfs = isset($req->chkMfs) ? 1 : 0;
        $msHoh = isset($req->chkHoh) ? 1 : 0;
        $msQw = isset($req->chkQw) ? 1 : 0;

        $sfname = $req->sfname;
        $slname = $req->slname;
        $sdob = $req->sdob;

        $country = $req->country;
        $state = $req->state;
        $city = $req->city;
        $address = $req->address;
        $notes = $req->profile_notes;
        
        $citizenship = $req->citizenship; //array
        $btnSave = $req->submit; 
        $allCountries = json_decode($req->allCountryIds);

//       print_r($allCountries);
//       die();

              $id = DB::table('tax_profile')->insertGetId(
                array('fname' => $fname, 'lname'=>$lname, 'dob'=>$dob, 'msSingle'=>$msSingle, 
                'msMfj' => $msMfj, 'msMfs'=>$msMfs, 'msHoh'=>$msHoh, 'msQw'=>$msQw,
                'sfname' => $sfname, 'slname'=>$slname, 'sdob'=>$sdob,'country' => $country, 
                'city'=>$city, 'state'=>$state,'address' => $address, 'citizenship'=>json_encode($citizenship), 'notes'=>$notes)
            );
        
        $depFname = $req->dfname; //array
        $depLname = $req->dlname; //array
        $relation = $req->dRelationship; //array
//         $chkChildTaxCredit = $req->chkChildTaxCredit;
//         $chkChildForOthrt = $req->get('chkChildForOthrt');
        
        $i = 0;
        foreach($depFname as $dfn){
          $chkChildTaxCredit = $req->get('chkChildTaxCredit'.$i);
          $chkChildForOthrt = $req->get('chkChildForOthrt'.$i);
          $chkChildTaxCredit = isset($chkChildTaxCredit) ? 1 : 0;
          $chkChildForOthrt = isset($chkChildForOthrt) ? 1 : 0;
            $id2 = DB::table('tax_profile_depn')->insertGetId(
                        array('fname' => $dfn, 'lname'=>$depLname[$i], 'relation'=>$relation[$i], 
                        'child_tax_credit'=>$chkChildTaxCredit, 'child_for_other'=>$chkChildForOthrt, 'tax_profile_id'=>$id));
            $i++;
        }
      
        $allCountries = json_decode($req->allCountryIds);
        $id2 = -1;
        foreach($allCountries as $con){
           $myCon = request('inc'.$con);
          
            $myCon =  isset($myCon) ? 1 : 0;
            if($myCon == 1){
                $arr = explode('-',$con);
                $incId = $arr[0];
                $cId = $arr[1];
              $docx = request($con.'input');
//               echo '<pre>';
//               print_r($docx);
                $id2 = DB::table('tax_income_type')->insertGetId(
                    array('country_id' => $cId, 'income_id'=>$incId, 'tax_profile_id'=>$id));
                $doc = $countryDocx = DB::table('income_type_docx')->where('income_type_id', '=', $incId)
                        ->where('country_id', '=', $cId)->get();
                foreach($docx as $d){
                  DB::table('tax_income_type_docx')->insertGetId(
                    array('docx' => $d, 'country_id'=>$cId, 'tax_income_type_id'=>$id2));
                }
            }
        }
      $extraDocx = $req->extraField;
      if(isset($extraDocx) && count($extraDocx) >0){
         $id3 = DB::table('tax_income_type')->insertGetId(
                      array('country_id' => -1, 'income_id'=>-1, 'tax_profile_id'=>$id));
      
      foreach($extraDocx as $ed){
        DB::table('tax_income_type_docx')->insertGetId(
                    array('docx' => $ed, 'country_id'=>-1, 'tax_income_type_id'=>$id3));
      }
      }
        if($btnSave == 'pdf'){
            return $this->profilePdf($id);
        }
      return redirect('profileDetail/'.$id);
//         return redirect('/taxProfile');
        
    }


    public function clientView(){
        $clientList =  DB::table('tax_profile')->get();
        return view('frontView.tax.clientView', compact('clientList'));
    }

    public function deleteProfile($id){
<<<<<<< HEAD
        DB::table('tax_profile')->where('id', '=', $id)->delete();
=======
      $tip =  DB::table('tax_income_type')->where('tax_profile_id', '=', $id)->get();
      foreach($tip as $t){
        DB::table('tax_income_type_docx')->where('tax_income_type_id', '=', $t->id)->delete(); 
      }
      DB::table('tax_income_type')->where('tax_profile_id', '=', $id)->delete();
      DB::table('tax_profile_depn')->where('tax_profile_id', '=', $id)->delete();
      DB::table('tax_profile')->where('id', '=', $id)->delete(); 
      return redirect('client');
>>>>>>> build 2
        
    }

    public function profileDetail($id){
        $client =  DB::table('tax_profile')->where('id', '=', $id)->first();
        $txtProfileDocList = DB::table('tax_income_type')->where('tax_profile_id', '=', $id)->get();
        foreach($txtProfileDocList as $tp){
          $cName = DB::table('countries')->select('country_name')->where('country_id', '=', $tp->country_id)->first();
          $allDoc = DB::table('tax_income_type_docx')->select('docx')
            ->where('country_id', '=', $tp->country_id)->where('tax_income_type_id', '=', $tp->id)->get();
          $temp = DB::table('income_type')->where('id', '=', $tp->income_id)->first();
          
//           $con = json_decode($temp->country_id);
//           $index =  array_search($tp->country_id, $con);
//           $docArray = json_decode($temp->documents);
//           $tp->dox = $docArray[$index];

          $tp->docx = $temp;
          $tp->country_name = $cName;
          $td = '<ul>';
          foreach($allDoc  as $ad){
            $td .= '<li>'.$ad->docx.'</li>';
          }
          $td .= '</ul>';
          $tp->dox = $td;
        }
//         echo '<pre>';
//         print_r($txtProfileDocList);
//         die();
        return view('frontView.tax.taxProfileDetail', compact('id','client','txtProfileDocList'));
    }

    public function profilePdf($id){
        $client =  DB::table('tax_profile')->where('id', '=', $id)->first();
        $txtProfileDocList = DB::table('tax_income_type')->where('tax_profile_id', '=', $id)->get();
        foreach($txtProfileDocList as $tp){
            $cName = DB::table('countries')->select('country_name')->where('country_id', '=', $tp->country_id)->first();
            $allDoc = DB::table('tax_income_type_docx')->select('docx')
              ->where('country_id', '=', $tp->country_id)->where('tax_income_type_id', '=', $tp->id)->get();
            $temp = DB::table('income_type')->where('id', '=', $tp->income_id)->first();
//             $con = json_decode($temp->country_id);
//             $index =  array_search($tp->country_id, $con);
//             $docArray = json_decode($temp->documents);
//             $tp->dox = $docArray[$index];
//             $tp->country_name = $cName;
//             $tp->docx = $temp;
          $tp->docx = $temp;
          $tp->country_name = $cName;
          $td = '<ul>';
          foreach($allDoc  as $ad){
            $td .= '<li>'.$ad->docx.'<div  style="border-style: solid;border-color: black;border-width:1px; width: 10px;float: right; height: 10px;"></div></li>';
          }
          $td .= '</ul>';
          $tp->dox = $td;
        }
        // return view('frontView.tax.profilePdf', compact('id','client','txtProfileDocList'));
        view()->share('txtProfileDocList',$txtProfileDocList);
        $pdf = PDF::loadView('frontView.tax.profilePdf', compact('id','client','txtProfileDocList'))->save('pdf_file.pdf');
        return $pdf->stream('pdf_file.pdf');
    }
  
  public function editProfile($id){
    $allCountry = $this->getCountryList();
    $client =  DB::table('tax_profile')->where('id', '=', $id)->first();
    $txtProfileDocList = DB::table('tax_income_type')->where('tax_profile_id', '=', $id)->get();
    $txtProfileDepList = DB::table('tax_profile_depn')->where('tax_profile_id', '=', $id)->get();

    foreach($txtProfileDocList as $tpd){
      $tpd->docx = DB::table('tax_income_type_docx')->where('tax_income_type_id', '=', $tpd->id)->where('country_id', '=', $tpd->country_id)->get();
      if($tpd->country_id != -1){
        $tpd->country_name = DB::table('countries')->where('country_id', '=', $tpd->country_id)->first()->country_name;
      }else{
        $tpd->country_name = '';
      }
      if($tpd->income_id != -1){
        $tpd->income_name = DB::table('income_type')->where('id', '=', $tpd->income_id)->first()->name;
      }else{
        $tpd->income_name = '';
      }
    }
//     echo '<pre>';
//       print_r($txtProfileDocList);
//     die();
    $incomeList =  DB::table('income_type')->get();
    $countryList =  DB::table('countries')
                    ->where('status', '=', 1)
                    ->select('*')
                     ->get();
    $countryListIds = array();
    foreach($incomeList as $in){
      
      $country = DB::table('income_type_docx')->where('income_type_id', '=', $in->id)->groupBy('country_id')->get();
      $cnt = array();
      $docxArray = array();
      foreach($country as $con){
        array_push($countryListIds, $in->id.'-'.$con->country_id);
        array_push($cnt , DB::table('countries')->where('country_id', '=', $con->country_id)->first());
        $countryDocx = DB::table('income_type_docx')->where('income_type_id', '=', $in->id)->where('country_id', '=', $con->country_id)->get();
        array_push($docxArray, json_encode($countryDocx));
        
      }
       $in->country = $cnt;
       $in->country_docx = $docxArray;
      
    }
//         echo '<pre>';
//         print_r($incomeList);die();
    return view('frontView.tax.editTaxProfile', compact('incomeList', 'countryListIds','countryList', 'client', 
                                                        'txtProfileDocList', 'txtProfileDepList', 'allCountry', 'id'));
  }
  
  
  public function updateProfile(Request $req){
    $updateUserId = $req->updateUserId;
    $fname = $req->fname;
    $lname = $req->lname;
    $dob = $req->dob;

    $msSingle = isset($req->chkSingle) ? 1 : 0;
    $msMfj = isset($req->chkMfj) ? 1 : 0;
    $msMfs = isset($req->chkMfs) ? 1 : 0;
    $msHoh = isset($req->chkHoh) ? 1 : 0;
    $msQw = isset($req->chkQw) ? 1 : 0;

    $sfname = $req->sfname;
    $slname = $req->slname;
    $sdob = $req->sdob;

    $country = $req->country;
    $state = $req->state;
    $city = $req->city;
    $address = $req->address;
    $notes = $req->profile_notes;

    $citizenship = $req->citizenship; //array
    $btnSave = $req->submit; 
    DB::table('tax_profile')
          ->where('id', $updateUserId)
          ->update(array('fname' => $fname, 'lname'=>$lname, 'dob'=>$dob, 'msSingle'=>$msSingle, 
            'msMfj' => $msMfj, 'msMfs'=>$msMfs, 'msHoh'=>$msHoh, 'msQw'=>$msQw,
            'sfname' => $sfname, 'slname'=>$slname, 'sdob'=>$sdob,'country' => $country,
            'city'=>$city, 'state'=>$state,'address' => $address, 'citizenship'=>json_encode($citizenship), 'notes'=> $notes ));
    
    $depFname = $req->dfname; //array
    $depLname = $req->dlname; //array
    $relation = $req->dRelationship; //array
//     $chkChildTaxCredit = $req->chkChildTaxCredit;
//     $chkChildForOthrt = $req->get('chkChildForOthrt');
    DB::table('tax_profile_depn')->where('tax_profile_id', '=', $updateUserId)->delete();
    $i = 0;
    foreach($depFname as $dfn){
      $chkChildTaxCredit = $req->get('chkChildTaxCredit'.$i);
      $chkChildForOthrt = $req->get('chkChildForOthrt'.$i);
      $chkChildTaxCredit = isset($chkChildTaxCredit) ? 1 : 0;
      $chkChildForOthrt = isset($chkChildForOthrt) ? 1 : 0;
        $id2 = DB::table('tax_profile_depn')->insertGetId(
                    array('fname' => $dfn, 'lname'=>$depLname[$i], 'relation'=>$relation[$i], 
                    'child_tax_credit'=>$chkChildTaxCredit, 'child_for_other'=>$chkChildForOthrt, 'tax_profile_id'=>$updateUserId));
        $i++;
    }

<<<<<<< HEAD
    public function profileDetail($id){
        $client =  DB::table('tax_profile')->where('id', '=', $id)->first();
        $txtProfileDocList = DB::table('tax_income_type')->where('tax_profile_id', '=', $id)->get();
        foreach($txtProfileDocList as $tp){
          $cName = DB::table('countries')->select('country_name')->where('country_id', '=', $tp->country_id)->first();
          $allDoc = DB::table('tax_income_type_docx')->select('docx')
            ->where('country_id', '=', $tp->country_id)->where('tax_income_type_id', '=', $tp->id)->get();
          $temp = DB::table('income_type')->where('id', '=', $tp->income_id)->first();
          
//           $con = json_decode($temp->country_id);
//           $index =  array_search($tp->country_id, $con);
//           $docArray = json_decode($temp->documents);
//           $tp->dox = $docArray[$index];

          $tp->docx = $temp;
          $tp->country_name = $cName;
          $td = '<ul>';
          foreach($allDoc  as $ad){
            $td .= '<li>'.$ad->docx.'</li>';
          }
          $td .= '</ul>';
          $tp->dox = $td;
        }
//         echo '<pre>';
//         print_r($txtProfileDocList);
//         die();
        return view('frontView.tax.taxProfileDetail', compact('id','client','txtProfileDocList'));
    }

    public function profilePdf($id){
        $client =  DB::table('tax_profile')->where('id', '=', $id)->first();
        $txtProfileDocList = DB::table('tax_income_type')->where('tax_profile_id', '=', $id)->get();
        foreach($txtProfileDocList as $tp){
            $cName = DB::table('countries')->select('country_name')->where('country_id', '=', $tp->country_id)->first();
            $allDoc = DB::table('tax_income_type_docx')->select('docx')
              ->where('country_id', '=', $tp->country_id)->where('tax_income_type_id', '=', $tp->id)->get();
            $temp = DB::table('income_type')->where('id', '=', $tp->income_id)->first();
//             $con = json_decode($temp->country_id);
//             $index =  array_search($tp->country_id, $con);
//             $docArray = json_decode($temp->documents);
//             $tp->dox = $docArray[$index];
//             $tp->country_name = $cName;
//             $tp->docx = $temp;
          $tp->docx = $temp;
          $tp->country_name = $cName;
          $td = '<ul>';
          foreach($allDoc  as $ad){
            $td .= '<li>'.$ad->docx.'<div  style="border-style: solid;border-color: black;border-width:1px; width: 10px;float: right; height: 10px;"></div></li>';
          }
          $td .= '</ul>';
          $tp->dox = $td;
        }
        // return view('frontView.tax.profilePdf', compact('id','client','txtProfileDocList'));
        view()->share('txtProfileDocList',$txtProfileDocList);
        $pdf = PDF::loadView('frontView.tax.profilePdf', compact('id','client','txtProfileDocList'))->save('pdf_file.pdf');
        return $pdf->stream('pdf_file.pdf');
    }
  
  public function editProfile($id){
    $allCountry = $this->getCountryList();
    $client =  DB::table('tax_profile')->where('id', '=', $id)->first();
    $txtProfileDocList = DB::table('tax_income_type')->where('tax_profile_id', '=', $id)->get();
    $txtProfileDepList = DB::table('tax_profile_depn')->where('tax_profile_id', '=', $id)->get();

    foreach($txtProfileDocList as $tpd){
      $tpd->docx = DB::table('tax_income_type_docx')->where('tax_income_type_id', '=', $tpd->id)->where('country_id', '=', $tpd->country_id)->get();
      if($tpd->country_id != -1){
        $tpd->country_name = DB::table('countries')->where('country_id', '=', $tpd->country_id)->first()->country_name;
      }else{
        $tpd->country_name = '';
      }
      if($tpd->income_id != -1){
        $tpd->income_name = DB::table('income_type')->where('id', '=', $tpd->income_id)->first()->name;
      }else{
        $tpd->income_name = '';
      }
    }
//     echo '<pre>';
//       print_r($txtProfileDocList);
//     die();
    $incomeList =  DB::table('income_type')->get();
    $countryList =  DB::table('countries')
                    ->where('status', '=', 1)
                    ->select('*')
                     ->get();
    $countryListIds = array();
    foreach($incomeList as $in){
      
      $country = DB::table('income_type_docx')->where('income_type_id', '=', $in->id)->groupBy('country_id')->get();
      $cnt = array();
      $docxArray = array();
      foreach($country as $con){
        array_push($countryListIds, $in->id.'-'.$con->country_id);
        array_push($cnt , DB::table('countries')->where('country_id', '=', $con->country_id)->first());
        $countryDocx = DB::table('income_type_docx')->where('income_type_id', '=', $in->id)->where('country_id', '=', $con->country_id)->get();
        array_push($docxArray, json_encode($countryDocx));
        
      }
       $in->country = $cnt;
       $in->country_docx = $docxArray;
      
    }
//         echo '<pre>';
//         print_r($incomeList);die();
    return view('frontView.tax.editTaxProfile', compact('incomeList', 'countryListIds','countryList', 'client', 
                                                        'txtProfileDocList', 'txtProfileDepList', 'allCountry', 'id'));
  }
  
  
  public function updateProfile(Request $req){
    $updateUserId = $req->updateUserId;
    $fname = $req->fname;
    $lname = $req->lname;
    $dob = $req->dob;

    $msSingle = isset($req->chkSingle) ? 1 : 0;
    $msMfj = isset($req->chkMfj) ? 1 : 0;
    $msMfs = isset($req->chkMfs) ? 1 : 0;
    $msHoh = isset($req->chkHoh) ? 1 : 0;
    $msQw = isset($req->chkQw) ? 1 : 0;

    $sfname = $req->sfname;
    $slname = $req->slname;
    $sdob = $req->sdob;

    $country = $req->country;
    $state = $req->state;
    $city = $req->city;
    $address = $req->address;
    $notes = $req->profile_notes;

    $citizenship = $req->citizenship; //array
    $btnSave = $req->submit; 
    DB::table('tax_profile')
          ->where('id', $updateUserId)
          ->update(array('fname' => $fname, 'lname'=>$lname, 'dob'=>$dob, 'msSingle'=>$msSingle, 
            'msMfj' => $msMfj, 'msMfs'=>$msMfs, 'msHoh'=>$msHoh, 'msQw'=>$msQw,
            'sfname' => $sfname, 'slname'=>$slname, 'sdob'=>$sdob,'country' => $country,
            'city'=>$city, 'state'=>$state,'address' => $address, 'citizenship'=>json_encode($citizenship), 'notes'=> $notes ));
    
    $depFname = $req->dfname; //array
    $depLname = $req->dlname; //array
    $relation = $req->dRelationship; //array
//     $chkChildTaxCredit = $req->chkChildTaxCredit;
//     $chkChildForOthrt = $req->get('chkChildForOthrt');
    DB::table('tax_profile_depn')->where('tax_profile_id', '=', $updateUserId)->delete();
    $i = 0;
    foreach($depFname as $dfn){
      $chkChildTaxCredit = $req->get('chkChildTaxCredit'.$i);
      $chkChildForOthrt = $req->get('chkChildForOthrt'.$i);
      $chkChildTaxCredit = isset($chkChildTaxCredit) ? 1 : 0;
      $chkChildForOthrt = isset($chkChildForOthrt) ? 1 : 0;
        $id2 = DB::table('tax_profile_depn')->insertGetId(
                    array('fname' => $dfn, 'lname'=>$depLname[$i], 'relation'=>$relation[$i], 
                    'child_tax_credit'=>$chkChildTaxCredit, 'child_for_other'=>$chkChildForOthrt, 'tax_profile_id'=>$updateUserId));
        $i++;
    }

=======
>>>>>>> build 2
    $txtProfileDocList = DB::table('tax_income_type')->where('tax_profile_id', '=', $updateUserId)->get();
    foreach($txtProfileDocList as $tpd){
      DB::table('tax_income_type_docx')->where('tax_income_type_id', '=', $tpd->id)->where('country_id', '=', $tpd->country_id)->delete();
    }
    DB::table('tax_income_type')->where('tax_profile_id', '=', $updateUserId)->delete();
        $allCountries = json_decode($req->allCountryIds);
        $id2 = -1;
        foreach($allCountries as $con){
           $myCon = request('inc'.$con);
          
            $myCon =  isset($myCon) ? 1 : 0;
            if($myCon == 1){
                $arr = explode('-',$con);
                $incId = $arr[0];
                $cId = $arr[1];
              $docx = request($con.'input');
<<<<<<< HEAD
              echo '<pre>';
              print_r($docx);
                $id2 = DB::table('tax_income_type')->insertGetId(
                    array('country_id' => $cId, 'income_id'=>$incId, 'tax_profile_id'=>$updateUserId));
                $doc = $countryDocx = DB::table('income_type_docx')->where('income_type_id', '=', $incId)
                        ->where('country_id', '=', $cId)->get();
                foreach($docx as $d){
                  DB::table('tax_income_type_docx')->insertGetId(
                    array('docx' => $d, 'country_id'=>$cId, 'tax_income_type_id'=>$id2));
=======
//               echo '<pre>';
//               print_r($docx);
              
//               foreach($docx as $ddd){
//                 echo $ddd;
//               }
//               die();
                $id2 = DB::table('tax_income_type')->insertGetId(array('country_id' => $cId, 'income_id'=>$incId, 'tax_profile_id'=>$updateUserId));
                $doc  = DB::table('income_type_docx')->where('income_type_id', '=', $incId)->where('country_id', '=', $cId)->get();
              if(isset($docx))
                foreach($docx as $ddd){
                  DB::table('tax_income_type_docx')->insertGetId(
                    array('docx' => $ddd, 'country_id'=>$cId, 'tax_income_type_id'=>$id2));
>>>>>>> build 2
                }
            }
        }
     $extraDocx = $req->extraField;
<<<<<<< HEAD
      print_r($extraDocx);
=======
//       print_r($extraDocx);
>>>>>>> build 2
    if(isset($extraDocx) && count($extraDocx) >0){
         $id3 = DB::table('tax_income_type')->insertGetId(
                      array('country_id' => -1, 'income_id'=>-1, 'tax_profile_id'=>$updateUserId));
      foreach($extraDocx as $ed){
        DB::table('tax_income_type_docx')->insertGetId(
                    array('docx' => $ed, 'country_id'=>-1, 'tax_income_type_id'=>$id3));
      }
    }
     if($btnSave == 'pdf'){
            return $this->profilePdf($updateUserId);
        }
    return redirect('profileDetail/'.$updateUserId);
//       return redirect('client');
  }
  
  
  public function getCountryList(){
    $countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
    return $countries;
  }

}
