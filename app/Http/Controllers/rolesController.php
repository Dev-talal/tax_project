<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RolesModel;
use DB;

class rolesController extends Controller{

    public function addNewUserRole(Request $req){
        $name = $req->name;
        
         $role['addClient'] = isset($req->addClient) ? 1 : 0;
         $role['editClient'] = isset($req->editClient) ? 1 : 0;
         $role['deleteClient'] = isset($req->deleteClient) ? 1 : 0;
         $role['updateClient'] = isset($req->updateClient) ? 1 : 0;
        if($role['addClient'] == 1 ||  $role['editClient'] == 1 ||$role['deleteClient'] == 1 || $role['updateClient'] == 1 ){
            $role['viewClient'] = 1;
        }else{
            $role['viewClient'] = 0;
        }

         $role['addRole'] = isset($req->addRole) ? 1 : 0;
         $role['editRole'] = isset($req->editRole) ? 1 : 0;
         $role['deleteRole'] = isset($req->deleteRole) ? 1 : 0;
         if($role['addRole'] == 1 ||  $role['editRole'] == 1 || $role['deleteRole'] == 1){
            $role['viewRole'] = 1;
        }else{
            $role['viewRole'] = 0;
        }

         $role['addForm'] = isset($req->addForm) ? 1 : 0;
         $role['editForm'] = isset($req->editForm) ? 1 : 0;
         $role['deleteForm'] = isset($req->deleteForm) ? 1 : 0;
         if($role['addForm'] == 1 ||  $role['editForm'] == 1 || $role['deleteForm'] == 1){
            $role['viewForm'] = 1;
        }else{
            $role['viewForm'] = 0;
        }
         $role['addAdmin'] = isset($req->addAdmin) ? 1 : 0;
         $role['editAdmin'] = isset($req->editAdmin) ? 1 : 0;
         $role['deleteAdmin'] = isset($req->deleteAdmin) ? 1 : 0;
         if($role['addAdmin'] == 1 ||  $role['editAdmin'] == 1 || $role['deleteAdmin'] == 1){
            $role['viewAdmin'] = 1;
         }else{
            $role['viewAdmin'] = 0;
            }
         $role['addIncomType'] = isset($req->addIncomType) ? 1 : 0;
         $role['editIncomType'] = isset($req->editIncomType) ? 1 : 0;
         $role['deleteIncomType'] = isset($req->deleteIncomType) ? 1 : 0;
         if($role['addIncomType'] == 1 ||  $role['editIncomType'] == 1 || $role['deleteIncomType'] == 1){
            $role['viewIncomType'] = 1;
         }else{
            $role['viewIncomType'] = 0;
            }
        $role['addIncomTypeCat'] = isset($req->addIncomType) ? 1 : 0;
         $role['editIncomTypeCat'] = isset($req->editIncomType) ? 1 : 0;
         $role['deleteIncomTypeCat'] = isset($req->deleteIncomType) ? 1 : 0;
         if($role['addIncomTypeCat'] == 1 ||  $role['editIncomTypeCat'] == 1 || $role['deleteIncomTypeCat'] == 1){
            $role['viewIncomTypeCat'] = 1;
         }else{
            $role['viewIncomTypeCat'] = 0;
            }
         $role['addCountry'] = isset($req->addCountry) ? 1 : 0;
         $role['editCountry'] = isset($req->editCountry) ? 1 : 0;
         $role['deleteCountry'] = isset($req->deleteCountry) ? 1 : 0;
         if($role['addCountry'] == 1 ||  $role['editCountry'] == 1 || $role['deleteCountry'] == 1){
            $role['viewCountry'] = 1;
         }else{
            $role['viewCountry'] = 0;
            }
         $roles = new RolesModel();
         $roles->name = $name;
         $roles->role_data = json_encode($role);
         $curTime = new \DateTime();
         $roles->create_at = $curTime->format("Y-m-d");
         $roles->save();
         return redirect('/roles');
     }


     public function userRoleView(){
        $roles = DB::table('roles')->get();
       foreach($roles as $role){
         $role->count = DB::table('users')->where('role', '=', $role->id)->count();
       }
         return view('frontView.UserRolesView',compact('roles'));
    }

    public function addUserRole(){
        return view('frontView.addNewRoleView');
    }

    public function deleteRole(Request $req, $id){
        $checkCounter = DB::table('users')->where('role', '=', $id)->count();
        if($checkCounter > 0){
            return back()->with('error', 'Unable to delete, Role Already in use!');
        }else{
            $data = $req->all();
            RolesModel::where('id', '=', $id)->delete($data);

            return redirect('/roles');
        }
    }
  
  public function editRole($id){
    $role = DB::table('roles')->where('id', '=', $id)->first();
    return view('frontView.userRolesEditView', compact('role', 'id'));
  }
  
  public function editRoleSave(Request $req){
    $name = $req->name;
        
         $role['addClient'] = isset($req->addClient) ? 1 : 0;
         $role['editClient'] = isset($req->editClient) ? 1 : 0;
         $role['deleteClient'] = isset($req->deleteClient) ? 1 : 0;
         $role['updateClient'] = isset($req->updateClient) ? 1 : 0;
        if($role['addClient'] == 1 ||  $role['editClient'] == 1 ||$role['deleteClient'] == 1 || $role['updateClient'] == 1 ){
            $role['viewClient'] = 1;
        }else{
            $role['viewClient'] = 0;
        }

         $role['addRole'] = isset($req->addRole) ? 1 : 0;
         $role['editRole'] = isset($req->editRole) ? 1 : 0;
         $role['deleteRole'] = isset($req->deleteRole) ? 1 : 0;
         if($role['addRole'] == 1 ||  $role['editRole'] == 1 || $role['deleteRole'] == 1){
            $role['viewRole'] = 1;
        }else{
            $role['viewRole'] = 0;
        }

         $role['addForm'] = isset($req->addForm) ? 1 : 0;
         $role['editForm'] = isset($req->editForm) ? 1 : 0;
         $role['deleteForm'] = isset($req->deleteForm) ? 1 : 0;
         if($role['addForm'] == 1 ||  $role['editForm'] == 1 || $role['deleteForm'] == 1){
            $role['viewForm'] = 1;
        }else{
            $role['viewForm'] = 0;
        }
         $role['addAdmin'] = isset($req->addAdmin) ? 1 : 0;
         $role['editAdmin'] = isset($req->editAdmin) ? 1 : 0;
         $role['deleteAdmin'] = isset($req->deleteAdmin) ? 1 : 0;
         if($role['addAdmin'] == 1 ||  $role['editAdmin'] == 1 || $role['deleteAdmin'] == 1){
            $role['viewAdmin'] = 1;
         }else{
            $role['viewAdmin'] = 0;
            }
         $role['addIncomType'] = isset($req->addIncomType) ? 1 : 0;
         $role['editIncomType'] = isset($req->editIncomType) ? 1 : 0;
         $role['deleteIncomType'] = isset($req->deleteIncomType) ? 1 : 0;
         if($role['addIncomType'] == 1 ||  $role['editIncomType'] == 1 || $role['deleteIncomType'] == 1){
            $role['viewIncomType'] = 1;
         }else{
            $role['viewIncomType'] = 0;
            }
        $role['addIncomTypeCat'] = isset($req->addIncomType) ? 1 : 0;
         $role['editIncomTypeCat'] = isset($req->editIncomType) ? 1 : 0;
         $role['deleteIncomTypeCat'] = isset($req->deleteIncomType) ? 1 : 0;
         if($role['addIncomTypeCat'] == 1 ||  $role['editIncomTypeCat'] == 1 || $role['deleteIncomTypeCat'] == 1){
            $role['viewIncomTypeCat'] = 1;
         }else{
            $role['viewIncomTypeCat'] = 0;
            }
         $role['addCountry'] = isset($req->addCountry) ? 1 : 0;
         $role['editCountry'] = isset($req->editCountry) ? 1 : 0;
         $role['deleteCountry'] = isset($req->deleteCountry) ? 1 : 0;
         if($role['addCountry'] == 1 ||  $role['editCountry'] == 1 || $role['deleteCountry'] == 1){
            $role['viewCountry'] = 1;
         }else{
            $role['viewCountry'] = 0;
            }
    $updateId = $req->updateId;
    $curTime = new \DateTime();
    $data = array('name'=>$name, 'role_data'=> json_encode($role), 'create_at'=>$curTime->format("Y-m-d"));
    DB::table('roles')
          ->where('id', $updateId)
          ->update($data);
         return redirect('/roles');
  }


}
