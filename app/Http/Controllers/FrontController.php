<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\frontModel;
use App\UsersModel;
use App\incomeTypeModel;
use App\formTypeModel;
use DB;

class FrontController extends Controller{
    public function index(){
        $totalUsers = DB::table('users')->count();
        $totalIncomeTypes = DB::table('income_type_category_name')->count();
        $totalFormTypes = DB::table('form_type')->count();
        $totalcountry = DB::table('countries')->count();
        $totalroles = DB::table('roles')->count();
        $totalClients = DB::table('tax_profile')->count();
        return view('frontView.home.homeView', compact('totalUsers','totalIncomeTypes','totalFormTypes','totalClients', 'totalroles','totalcountry'));
    }
    
    public function countries(Request $req){
        $countries = new frontModel;
        $countryList =  $countries::all();
         return view('frontView.countriesView',compact('countryList'));
    }

    public function changeCountryStatus(Request $req){
        $id =  $req->i;
        $status =  $req->s;
        if($status == 0){
            $status = 1;
        }else{
            $status = 0;
        }
        DB::table('countries')
              ->where('country_id', $id)
              ->update(['status' => $status]);
        return 
        redirect('/country');
        
    }

    public function addCountries(Request $req){

        $curTime = new \DateTime();
        $created_at = $curTime->format("Y-m-d");
        $name = $req->name;
        $checkcounter = DB::table('countries')->where('country_name', '=', $name)->count();
         if($checkcounter > 0){
           return back()->with('error', $name.' Already in use ..');
         }
      else{
        $countries = new frontModel;
        $countries->country_name = $name;
        $countries->created_at = $created_at;
        $countries->status = 1;
        $countries->save();
<<<<<<< HEAD

        return back()->with('success', 'Data added successfully..');
        
=======
        return back()->with('success', 'Data added successfully..');
      }
>>>>>>> build 2
    }

    public function showaddCountries(){
        return view('frontView.addCountryView');   
    }

    public function deleteCountry(Request $req,$country_id)
    {
        $checkCountry = DB::table('income_type_docx')->where('country_id', '=', $country_id)->count();
<<<<<<< HEAD
//         $checkCountry += DB::table('tax_income_type')->where('country_id', '=', $country_id)->count();
=======
        $checkCountry += DB::table('tax_income_type')->where('country_id', '=', $country_id)->count();
>>>>>>> build 2
//         $checkCountry += DB::table('tax_income_type')->where('country_id', '=', $country_id)->count();
        if($checkCountry > 0){
            return back()->with('error', 'Unable to delete, Country Already in use!');
        }else{
            $data = $req->all();
            frontModel::where('country_id', '=', $country_id)->delete($data);
            return redirect('/country');
        }
    }

   

    public function formType(Request $req){
        $form_type = new formTypeModel;
        $form_type_list =  $form_type::all();
         return view('frontView.formTypeView',compact('form_type_list'));
    }

    public function showformTypePage(){
        return view('frontView.addFormType');
   }

   public function addFormType(Request $req){

    $curTime = new \DateTime();
    $created_at = $curTime->format("Y-m-d");
<<<<<<< HEAD

    $form_type = new  formTypeModel;
    $form_type->form_type_name = $req->name;
    $form_type->created_at = $created_at;
    $form_type->save();
    return back()->with('success', 'Data added successfully..');
    
=======
    $name = $req->name;

     $checkcounter = DB::table('form_type')->where('form_type_name', '=', $name)->count();
     if($checkcounter > 0){
       return back()->with('error', $name.' Already in use ..');
     }else{ 
        $form_type = new  formTypeModel;
        $form_type->form_type_name = $name;
        $form_type->created_at = $created_at;
        $form_type->save();
        return back()->with('success', 'Data added successfully..');
     }    
>>>>>>> build 2
}

public function deleteFormType(Request $req,$form_id)
    {
        $checkcounter = DB::table('income_type')->where('form_type', '=', $form_id)->count();
        if($checkcounter > 0){
            return back()->with('error', 'Unable to delete, Form Type Already in use!');
        }else{
            $data = $req->all();
            formTypeModel::where('form_type_id', '=', $form_id)->delete($data);

            return redirect('/formType');
        }
    }

    public function updateCountry(Request $req)

    {

        frontModel::where('country_id',$req->country_id)->update(array(
        'country_name'=>$req->country_name));
        return redirect('/country');
    }



    //khuram start

    public function adminUsers(){
        $userList = DB::table('users')->select('users.*', 'roles.name as role_name', 'roles.id as role_id')->join('roles', 'roles.id', '=', 'users.role')->get();
        $roleList = DB::table('roles')->select('*')->get();
        return view('frontView.adminUsersView', compact('userList', 'roleList'));
    }

    public function addAdminUsers(){
        $roleList = DB::table('roles')->select('*')->get();
        return view('frontView.addAdminUser', compact('roleList'));
    }

    public function saveUser(){
        $user = new UsersModel();
        $user->fname = request('fname');
        $user->lname = request('lname');
        $user->email = request('email');
        $user->password = request('password');
        $user->role = request('role');
        $user->save();
        return redirect('/users');
        
    }

    public function deleteAdminUsers(Request $req, $id){
        $data = $req->all();
        UsersModel::where('id', '=', $id)->delete($data);

        return redirect('/users');
    }

    public function userRoleView(){
        return view('frontView.UserRolesView');
    }

    public function addUserRole(){
        return view('frontView.addNewRoleView');
    }
}
