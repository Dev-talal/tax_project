<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\frontModel;
use App\UsersModel;
use App\incomeTypeModel;
use App\formTypeModel;
use App\IncomeTypeDataModel;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

class incomeController extends Controller{

    public function incomeTypeCategories(){
        $income_type = new incomeTypeModel;
        $income_type_list =  $income_type::all();
         return view('frontView.incomeTypeView',compact('income_type_list'));
    }

    public function showIncomeTypeForm(){
        return view('frontView.addIncomeType');
   }

   public function addIncomeType(Request $req){

    $curTime = new \DateTime();
    $created_at = $curTime->format("Y-m-d");
    $name = $req->name;
    $checkcounter = DB::table('income_type_category_name')->where('income_type_name', '=', $name)->count();
     if($checkcounter > 0){
       return back()->with('error', $name.' Already in use ..');
     }
     
    $income_type = new incomeTypeModel;
    $income_type->income_type_name = $name;
    $income_type->created_at = $created_at;
    $income_type->save();
    return back()->with('success', 'Data added successfully..');
    
}
  
  public function checkIncomeType(Request $req){
    $name = $req->name;
    echo $checkcounter = DB::table('income_type')->where('name', '=', $name)->count();  
  }

public function deleteIncomeType(Request $req,$income_id){
        $checkcounter = DB::table('income_type')->where('income_type_id', '=', $income_id)->count();
        if($checkcounter > 0){
            return back()->with('error', 'Unable to delete, Income Type Already in use!');
        }else{
            $data = $req->all();
            incomeTypeModel::where('income_type_id', '=', $income_id)->delete($data);
            return redirect('/incomeTypeCategory');
        }
    }

    public function addIncomeTypeData(){
        return view('frontView.addIncomTypeView');   
    }


    public function incomeType(){
        $countryList =  DB::table('countries')
        ->where('status', '=', 1)
        ->select('*')
         ->get();
        $incomeTypeList =  incomeTypeModel::all();
        $formTypeList =  formTypeModel::all();
        return view('frontView.addIncomeTypeDataView', compact('countryList', 'incomeTypeList', 'formTypeList'));
    }

    public function incomeTypeDataView(){
        // $incomeList =  IncomeTypeDataModel::all();
        $countryList =  DB::table('countries')
                        ->where('status', '=', 1)
                        ->select('*')
                         ->get();
        $incomeTypeList =  incomeTypeModel::all();
        $formTypeList =  formTypeModel::all();
        $incomeList =  DB::table('income_type')
        ->join('form_type', 'income_type.form_type', '=', 'form_type.form_type_id')
        // ->join('countries', 'income_type.country_id', '=', 'countries.country_id')
        ->join('income_type_category_name', 'income_type.income_type_id', '=', 'income_type_category_name.income_type_id')
        ->select('income_type.*', 'form_type.form_type_name', 'income_type_category_name.income_type_name')->get();
        return view('frontView.incomeTypeDataView', compact('incomeList','countryList', 'incomeTypeList', 'formTypeList'));   
    }

    public function addNewIncomeTypeData(Request $req){
        $incomeCategory = $req->incomeTypeCategory;
        $incomType = $req->incomeType;
        $formType = $req->formType;
        $minFee = $req->minFee;
        $maxFee = $req->maxFee;
        $country = $req->country;
        $addMoreDocx = $req->addMoreDocx;
        $addMoreDocx = json_decode($addMoreDocx);
<<<<<<< HEAD

=======
        $checkcounter = DB::table('income_type')->where('name', '=', $incomType)->count();
        if($checkcounter > 0){
          return back()->with('error', $incomType.' Already in use ..');
        }
>>>>>>> build 2
        $url = 'placeholder.png';
        if($req->image != null){
          $extension = $req->image->extension();
          $img64 = $req->img64;
          $data = explode( ',', $img64);
          $data = base64_decode($data[1]);
          $image_name= time().'.'.$extension;
          $path = storage_path('app/'.$image_name);
          file_put_contents($path, $data);
          $url = $image_name;
        }
      
       $id = DB::table('income_type')->insertGetId(
                array('name' => $incomType, 'income_type_id'=>$incomeCategory, 'form_type'=>$formType,
                      'min_fee'=>$minFee, 'max_fee' => $maxFee, 'img_url'=>$url));
        $i=0;
        foreach($addMoreDocx as $dc){
          $documents = request($dc.'ducuments');
          $c = $country[$i];
          if(isset($c)){
            foreach($documents as $doc){
              DB::table('income_type_docx')->insertGetId(
                  array('country_id' => $c, 'docs'=>$doc, 'income_type_id'=>$id));
            }
          }
          $i++;
        }
      
//         $income_type = new IncomeTypeDataModel;
//         $income_type->name = $incomType;
//         $income_type->income_type_id = $incomeCategory;
//         $income_type->form_type = $formType;
//         $income_type->min_fee = $minFee;
//         $income_type->max_fee = $maxFee;
//         $income_type->country_id = json_encode($country);
//         $income_type->documents = json_encode($documents);
       
//         if ($req->hasFile('image')) {
//             if ($req->file('image')->isValid()) {
//                 //
//                 $validated = $req->validate([
//                     'image' => 'mimes:jpeg,png|max:1014',
//                 ]);
//                 $extension = $req->image->extension();
//                 $imgName = time();
//                 $req->image->storeAs('/', $imgName.".".$extension);
//                 $url = $imgName.".".$extension;
//             }
        
//         }
//         $income_type->img_url = $url;
//         $income_type->save();
        // print_r($documents);
        return redirect('/incomeTypeView');
    }

    public function deleteIncomeData(Request $req,$id){
        $data = $req->all();
        $checkcounter = DB::table('tax_income_type')->where('income_id', '=', $id)->count();
        if($checkcounter > 0){
           return back()->with('error', 'Unable To delete');
         }
        IncomeTypeDataModel::where('id', '=', $id)->delete($data);
        DB::table('income_type_docx')->where('income_type_id', '=', $id)->delete();
        return redirect('/incomeTypeView');
    }
  
  public function editIncomeData($id){
     $countryList =  DB::table('countries')
        ->where('status', '=', 1)
        ->select('*')
         ->get();
        $incomeTypeList =  incomeTypeModel::all();
        $formTypeList =  formTypeModel::all();
        $incomeTypeData = DB::table('income_type')->select('*')->where('id','=', $id)->first();
    
    $incomeTypeDocx = DB::table('income_type_docx')->select('country_id')->where('income_type_id','=', $id)->groupBy('country_id')->get();
    foreach($incomeTypeDocx as $inc){
      $temp = DB::table('income_type_docx')->select('*')->where('income_type_id','=', $id)->where('country_id', '=', $inc->country_id)->get();
      $inc->docx = $temp;
    }
//     echo '<pre>';
//     print_r($incomeTypeDocx);
//     die();
        return view('frontView.editIncomeTypeDataView', compact('id','countryList', 'incomeTypeList', 'formTypeList','incomeTypeData','incomeTypeDocx'));
  }
  
      public function editIncomeDataSave(Request $req){
        $incomeCategory = $req->incomeTypeCategory;
        $incomType = $req->incomeType;
        $formType = $req->formType;
        $minFee = $req->minFee;
        $maxFee = $req->maxFee;
        $country = $req->country;
        $addMoreDocx = $req->countryCounter;
        echo $incomeId = $req->incomeId;
        
        $data = array('name' => $incomType, 'income_type_id'=>$incomeCategory, 'form_type'=>$formType,
                      'min_fee'=>$minFee, 'max_fee' => $maxFee);

        $url = 'placeholder.png';
        if($req->image != null){
          $extension = $req->image->extension();
          $img64 = $req->img64;
          $data = explode( ',', $img64);
          $data = base64_decode($data[1]);
          $image_name= time().'.'.$extension;
          $path = storage_path('app/'.$image_name);
          file_put_contents($path, $data);
          $url = $image_name;
          $data = array('name' => $incomType, 'income_type_id'=>$incomeCategory, 'form_type'=>$formType,
                      'min_fee'=>$minFee, 'max_fee' => $maxFee, 'img_url'=>$url);
        }
        
        DB::table('income_type')->where('id', $incomeId)->update($data);
         DB::table('income_type_docx')->where('income_type_id', '=', $incomeId)->delete();
        for($i = 1; $i<=$addMoreDocx; $i++){
          $documents = request($i.'ducuments');
          print_r($documents);
          $c = $country[$i-1];
          if(isset($c)){
            foreach($documents as $doc){
              DB::table('income_type_docx')->insertGetId(
                  array('country_id' => $c, 'docs'=>$doc, 'income_type_id'=>$incomeId));
            }
          }
        }
        
        return redirect('/incomeTypeView');
    }

}