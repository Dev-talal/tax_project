<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolesModel extends Model
{
    protected  $table = 'roles';
    protected $fillable = [
        'name','role_Data', 'create_at'
    ];
    public $timestamps = false;
}
